'use strict';

new WOW().init();

var searchTerm = '';
var search;

var screenPlayQuizContent;

var currentQuiz;
var questionList;
var quizData = {};
var currentQuestion;
var questionList = {};
var playerAnswers = [];
var options = ['a','b','c','d','e','f','g','h','i','j'];
var selectedOption = '';
var playerScore;

$(document).ready(function () {

    loadScreen('#screen_start_quiz');

    $('#Search').select2({
	    placeholder: "Search for a repository",
	    minimumInputLength: 1,
	    query: function (query) {
	    	search =  query;
	    	APIRequest('search/quiz', query.term + '/5' ,'loadDataResults');     
	    },
	    openOnEnter:true
	});

    $('#Search').change(function(){
        getQuiz($('#Search').val());
    });

    $('#postSubmitQuiz').submit(function() {
        if($('#Search').val() !== ''){
            loadScreen('#screen_play_quiz');
            $(".wrap-personal").animate({top:'20%'}).css({'box-shadow':'0 0 40px 0px #E8C5B3'});
            //$("body").css({background:'#E8C5B3'});
            $('.header').hide();
            $('.play-quiz-header').show();
        }
        return false;
    });

    $('#startQuizButton').click(function() {
        $("#postSubmitQuiz").trigger('submit');
    });

    screenPlayQuizContent = $('#screen_play_quiz').html();
});

// Search results
function loadDataResults(a){
	var quizList = a.MSG;
	var data = {results: []};
    for (var i = 0; i < quizList.length; i++) {
        data.results.push({id: quizList[i].quiz_id, text: quizList[i].quiz_name});
    }
    search.callback(data);
}


// get quiz by quiz_key
function getQuiz(quiz_id){
    APIRequest(
        'quiz/info',
        quiz_id,
        'setQuiz'
        );

    currentQuiz = quiz_id;
}

// load quiz
function setQuiz(a){
    if(a.STATUS === 'OK'){
        quizData = a.MSG;

        var screenPlayQuizContentNew = screenPlayQuizContent.replace(/\[(\w+)\]/g, function(s, key) { return quizData[key] || s; });
        $('#screen_play_quiz').html(screenPlayQuizContentNew);

        $('.play-quiz-header h1').html(quizData.quiz_name);

        $('#screen_play_quiz').data('title',quizData.quiz_name + ' - Quiz');
        $('#screen_play_quiz').data('history','q/'+quizData.quiz_key);

        $(".backgroundImage").attr('src',quizData.image);

        $('#postSubmitName').submit(function() {
            var name = $('#NameInput').val();
            if(name !== ''){
                $('#NameInput').blur();
                $('#startQuiz').hide();
                $('#showQuestion').fadeIn();
                answersContent = $('#answersContent').html();

                $('#answersContent').showLoadScreen();

                APIRequest(
                    'quiz/start',
                    currentQuiz+'/'+name+'/en',
                    'startQuiz'
                    );
            } else {
                //$('.fillInName').alertObject();
            }
            return false;
        });

        $("#triggerPostSubmitName").click(function() {
            $("#postSubmitName").trigger('submit');
        });
    }
}

function startQuiz(get){
    var getData = {};
    getData = get.MSG;

    questionList = getData.question_list;

    quizData.numberOfQuestions = questionList.length;
    //$(".totalQuestions").html(quizData.numberOfQuestions);

    quizData.player_id = getData.player_id;

    startIndividualQuiz();
}




// individual quiz

function startIndividualQuiz(){
    
    currentQuestion = 1;
    helper_loadquestion(currentQuestion);

    loadScreen('#screen_question');

    $('#showAnswerButton').off().on('click',function(){
        if(selectedOption === ''){
            alert('Please select an answer.');
        } else {
            playerAnswers[(currentQuestion-1)] = selectedOption;

            helper_submitanswer(currentQuestion,selectedOption);


            if(selectedOption === questionList[(currentQuestion-1)].question_answer){
                // answer was right
                $('.option_'+selectedOption).removeClass('selected').addClass('good');
                $('#questionFeedback').html('Congratulations, this answer was <strong>correct</strong>!')
                playerScore++;
            } else {
                // answer was wrong
                $('.option_'+selectedOption).removeClass('selected').addClass('wrong');
                var rightAnswer = questionList[(currentQuestion-1)]['question_answer_'+questionList[(currentQuestion-1)].question_answer];
                $('#questionFeedback').html('Not correct! The right answer was <strong>'+rightAnswer+'</strong>.')
            }

            $('#showAnswerButton').hide();

            $('#showExplanationButton').show();
        }
    });

    $('#nextQuestionButton').off().click(function() {
        $('#questionFeedback').html('');
        currentQuestion++;

        if(currentQuestion <= quizData.numberOfQuestions){
            // load next question
            helper_loadquestion(currentQuestion);
        } else {
            // quiz finished
            endIndividualQuiz();
        }
    });

}

function endIndividualQuiz(){

    loadScreen('#screen_results');

    $(".finalScore").html(playerScore);

    APIRequest(
        'quiz/finishIndividualQuiz',
        currentQuiz+'/'+quizData.player_id,
        'console.log'
        );
}


function loadScreen(s){
    $(".loadScreen").hide();
    $(s).slideDown();

    var title = null;
    
    if($(s).data('title')){
        title = $(s).data('title');
        $("title").html(title);
    }
    
    if($(s).data('history')){
        //history.pushState(null, title, $(s).data('history'));
    }
}








// HELPERS

function helper_loadquestion(question){
    var questionKey = (question-1);
    
    var newAnswersContent = '';


    $(".currentQuestion").html(question);

    $(".questionHeader").hide().html(questionList[questionKey]['question']).fadeIn(700);

    //$("#answersContent").showLoadScreen();

    var currentOption = 0;
    //alert(questionList[questionKey]['question_answer_'+options[currentOption]]);

    while(questionList[questionKey]['question_answer_'+options[currentOption]] !== ''){
        var optionInfo = {};
        optionInfo['questionLetter'] = options[currentOption];
        optionInfo['questionContent'] = questionList[questionKey]['question_answer_'+options[currentOption]];

        newAnswersContent = newAnswersContent+answersContent.replace(/\[(\w+)\]/g, function(s, key) { return optionInfo[key] || s; });
        
        currentOption++;
    }

    $("#answersContent").html(newAnswersContent).fadeIn(700);


/*
    // submit old answer
    if(selectedOption === '')
        selectedOption = 'x';
    helper_submitanswer(questionKey,selectedOption);
*/

    // reset
    selectedOption = '';
    $(".questionOption").off().on('click',function(){
        $(".questionOption").removeClass('selected');
        $(this).addClass('selected');
        selectedOption = $(this).data('question');
    });

}

function helper_submitanswer(question,answer){
    APIRequest(
        'quiz/submitAnswer',
        currentQuiz+'/'+quizData.player_id+'/'+question+'/'+answer,
        'console.log'
        );
}



/*
function loadResults(a) {

    if(a.STATUS == 'ERROR'){
        // no results
        $("#row").hide();
        $("#Error").html(Lang.NoSearchResults).fadeIn();
    } else {
        var quizList = a.MSG;
        var count = 0;
        var styles = ['blue','green','orange','red','purple'];
        var quizImg;

        $("#row").slideUp().html('');


        for (var b in quizList) {
            quizImg = '<img src="img/smallcat/' + quizList[b]["image"] + '"/>';
            if(quizList[b]["image"] == 'algemeen.png')
                 quizImg = '';
            $("#row").append('<div class="quiz" rel="' + quizList[b]["quiz_id"] + '"><h3>' + quizList[b]["quiz_name"] + "</h3><p>" + quizList[b]["user_name"] + "</p></div>");
            count = (count+1)%5;
        }
        $("#row").slideDown();

        $(".quiz").click(function () {
            currentQuiz = $(this).attr("rel");
            loadView("Play", "false");
            addToHistory('loadView','Search');
        });

    }
    setTimeout(function(){refreshScroll();},500);
}*/