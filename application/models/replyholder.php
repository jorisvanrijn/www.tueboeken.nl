<?php

	class Replyholder extends CI_Model{
		
		public $replies;
		
		public function construct(){
			parent::__construct();
			
			$this->load->model('reply','',TRUE);
		}
		
		public function getReplies(){
			return $this->replies;
		}
		
		public function getRepliesFromUser($id){
			
			$this->db->select('id');
			$q = $this->db->get_where('reply', array('seller_user_id' => $id), 3, 0);
			$replies = $q->result_array(); 
			
			foreach($replies as $reply){
				$id = ((int) $reply['id']);
				$this->reply->get(array('id' => $id));
				$this->replies[] = $this->reply->data;
			}
			
		}
		
		public function getOutgoingRepliesFromUser($id){
			
			$this->db->select('id');
			$users = $this->db->get_where('reply', array('user_id' => $id));
			
		}
		
		public function getRepliesFromBook($id){
			
		}
		
	}

?>