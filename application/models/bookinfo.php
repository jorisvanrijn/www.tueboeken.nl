<?php
Class BookInfo extends CI_Model
{

  public $id;
  public $bookArray;

  function __construct(){
    parent::__construct();
    $this->lang->load('book_status');
    $this->lang->load('book_state');
  }

 function get($where = array()){
    $where['site_id'] = $this->config->item('site_id');
    $this->db->select('id, title, subtitle, description, author, slug, image');

    $bookRes = $this->db->get_where('book',$where);
    
    if($bookRes->num_rows() > 0){
      $this->bookArray = $bookRes->row_array();
      $this->id = $this->bookArray['id'];

      return $this->bookArray;

    } else {
      return false;
    }
    return false;
  }

  function isbnList(){
    $this->db->select('name, value');
    $isbnRes = $this->db->get_where('book_isbn',array('site_id'=>$this->config->item('site_id'), 'book_id'=>$this->id));
    return $isbnRes->result_array();
  }

  function courseList(){
    $this->db->select('code');
    $courseRes = $this->db->get_where('tue_book_course',array('book_id'=>$this->id));
    return $courseRes->result_array();
  }

  function advertisements_count($isforsale = 1, $status_id = array(1)){
    $this->db->select('id');

    $find = array('site_id'=>$this->config->item('site_id'), 'book_id'=>$this->id, 'isforsale'=>$isforsale);
    
    $this->db->where_in('status_id', $status_id);
    
    $adRes = $this->db->get_where('advertisement',$find);
    
    return $adRes->num_rows();
  }

  function advertisements($isforsale = 1, $status_id = array(1), $order){
    $this->db->select('id, user_id, isforsale, price, status_id, state_id, description, created, lastchange');

    $find = array('site_id'=>$this->config->item('site_id'), 'book_id'=>$this->id, 'isforsale'=>$isforsale);
    
    $this->db->where_in('status_id', $status_id);
    
    $this->db->order_by($order);

    $adRes = $this->db->get_where('advertisement',$find);

    $adArray = array();
    $i = 0;

    foreach($adRes->result_array() as $ad){
      $adArray[$i] = $ad;
      
      // price
      setlocale(LC_MONETARY, 'en_US');
      $adArray[$i]['price'] = money_format('%i', $adArray[$i]['price']);

      // get user
      $this->db->select('name');
      $userRes = $this->db->get_where('user',array('id'=>$ad['user_id']));
      $user = $userRes->row_array();
      $adArray[$i]['name'] = $user['name'];

      // get state
      $this->db->select('languagekey');
      $stateRes = $this->db->get_where('state',array('id'=>$ad['state_id']));
      $state = $stateRes->row_array();
      $adArray[$i]['state'] = lang($state['languagekey']);

      // ad url
      $adArray[$i]['url'] = base_url('book/'.$this->bookArray['slug'].'/'.$ad['id']);

      $i++;
    }

    return $adArray;
  }
}
?>