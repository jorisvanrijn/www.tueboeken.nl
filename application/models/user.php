<?php
Class User extends CI_Model
{
 function login($username, $password)
 {
   $this -> db -> select('id, name, email, password, phone, startyear, description');
   $this -> db -> from('user');
   $this -> db -> where('site_id',$this->config->item('site_id'));
   $this -> db -> where('email', $username);
   $this -> db -> where('password', salt_password($password));
   $this -> db -> limit(1);

   $query = $this -> db -> get();


   if($query -> num_rows() == 1)
   {
     return $query->row_array();
   }
   else
   {
     return false;
   }
 }

 function checkLogin(){
    if($this->session->userdata('logged_in')) {

      $session_data = $this->session->userdata('logged_in');

      $this -> db -> select('*');
      $this -> db -> from('user');
      $this -> db -> where('site_id',$this->config->item('site_id'));
      $this -> db -> where('email', $session_data['email']);
      $this -> db -> where('password', $session_data['uuid']);
      $this -> db -> limit(1);

      $query = $this -> db -> get();

      if($query -> num_rows() == 1) {
        return $query->row_array();
      } else {
        return false;
      }

    } else {
      return false;
    }



  }

  function register($name,$password,$email){
    $userInfo = array();
    $userInfo['name'] = $name;
    $userInfo['email'] = $email;
    $userInfo['password'] = salt_password($password);
    $userInfo['site_id'] = $this->config->item('site_id');
    $this->db->insert('user',$userInfo);

    // big data
    $userData = array();
    $userData['user_id'] = $this->db->insert_id();
    $userData['action'] = 'register';
    $userData['ip'] = $_SERVER['REMOTE_ADDR'];
    $userData['browser'] = $_SERVER['HTTP_USER_AGENT'];
    $userData['header'] = json_encode($_SERVER);
    $userData['site_id'] = $this->config->item('site_id');
    $this->db->insert('user_data',$userData);

    // now sent a nice email
    $this->load->library('email');
    $this->email->from(lang('settings_contact_from'), lang('settings_contact_from_name'));
    $this->email->to($userInfo['email']);

    $find = array('[name]','[email]','[password]');
    $replace = array($name,$email,$password);

    $this->email->subject(str_replace($find,$replace,lang('home_register_email_subject')));
    $this->email->message(str_replace($find,$replace,lang('home_register_email_message'))); 

    $this->email->send();

    return true;
  }

  function resetPassword($e){
    $this->db->select('id, name, email');
    $this->db->where('site_id',$this->config->item('site_id'));
    $user = $this->db->get_where('user', array('email'=>$e));


   if($user->num_rows() > 0){

      $user = $user->row_array();


      $name = $user['name'];
      $email = $user['email'];

      // generate new password
      $this->load->helper('string');
      $password = random_string('alnum',rand(7,11));


      $userInfo = array();
      $userInfo['password'] = salt_password($password);

      // update
      $this->db->where(array('email'=>$email, 'id'=>$user['id']));
      $this->db->update('user', $userInfo);

      // now sent a nice email

      $find = array('[name]','[email]','[password]');
      $replace = array($name,$email,$password);

      $this->load->library('email');
      $this->email->from(lang('settings_contact_from'), lang('settings_contact_from_name'));
      $this->email->to($user['email']);

      $this->email->subject(str_replace($find,$replace,lang('home_forgot_email_subject')));
      $this->email->message(str_replace($find,$replace,lang('home_forgot_email_message'))); 

      $this->email->send();

      return true;
    } else {
      return false;
    }
  }
}
?>