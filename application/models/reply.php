<?php

	class Reply extends CI_Model{
		
		//public $id, $site_id, $advertisement_id, $user_id, $seller_user_id, $message, $created;
		public $data;
		
		public function construct(){
			parent::__construct();
			$this->site_id = $this->config->item('site_id');
		}
		
		public function get($where){
			$q = $this->db->get_where('reply', $where);
			$data = $q->row_array();
			
			/* $bookRes = $this->db->get_where('book', array('id'=>$data['ad_id']));
			
			if($bookRes->num_rows() != 0){
				$data['book'] = $bookRes->row_array();
			} */
			
			$this->data = $data;
		}
		
	}

?>