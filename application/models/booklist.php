<?php
Class BookList extends CI_Model
{

  public $searchQuery;
  public $rows;
  public $slice;
  public $courseCodes;
  public $bookIdList = array();

  function __construct(){
    parent::__construct();
    $this->lang->load('book_status');
    $this->lang->load('book_state');
    $this->load->helper('text');

    $searchQuery = '';
  }

  function search($q){
    $this->searchQuery = "SELECT *, MATCH(`title`,`subtitle`,`author`,`description`)
          
            AGAINST('".$this->db->escape_like_str($q)."') AS SCORE FROM book

            WHERE book.id IN (

              SELECT advertisement.book_id FROM advertisement WHERE advertisement.user_id IN (SELECT user.id FROM user WHERE user.name LIKE '%".$this->db->escape_like_str($q)."%')

            )

            OR

            MATCH(`title`,`subtitle`,`author`,`description`) AGAINST('".$this->db->escape_like_str($q)."')

            OR

            book.id IN (

              SELECT tue_book_course.book_id FROM tue_book_course WHERE tue_book_course.code LIKE '%".$this->db->escape_like_str($q)."%'

            )

            OR

            book.id IN (

              SELECT book_isbn.book_id FROM book_isbn WHERE book_isbn.value LIKE '%".$this->db->escape_like_str($q)."%'

            )
            

            ORDER BY SCORE DESC";
  }

  function slice_results($limit, $offset){
    $this->slice = array($limit,$offset);
  }

  function noSearch(){
    $this->searchQuery = '';
  }

  function customQuery($q){
    $this->searchQuery = $q;
  }

 function get($where = array(), $limit = null, $offset = null, $order = null){
    
    if($this->searchQuery == ''){
      $where['site_id'] = $this->config->item('site_id');
      $this->db->select('id, slug, title, subtitle, description, author, slug, image');
      if($order != '')
        $this->db->order_by($order);
      $bookRes = $this->db->get_where('book',$where,$limit,$offset);
    } else {
      $bookRes = $this->db->query($this->searchQuery);
    }

    $this->rows = $bookRes->num_rows();

    
    if($this->rows > 0){
      $bookArray = array();
      $i = 0;

      $arrOutput = $bookRes->result_array();

      $this->courseCodes = array();

      foreach($arrOutput as $bk){ // get a list of all book id's
        $this->bookIdList[] = $bk['id'];
      }

      $this->db->select('code');
      $this->db->where_in('book_id',$this->bookIdList);
      $courseRes = $this->db->get('tue_book_course');

      if($courseRes->num_rows() > 0){
        foreach($courseRes->result_array() as $course){
          if(!in_array($course['code'], $this->courseCodes)){
            $this->courseCodes[] = $course['code'];
          }
        }
      }

      if(is_array($this->slice)){
        $arrOutput = array_slice($arrOutput, $this->slice[1], $this->slice[0]);
      }

      foreach($arrOutput as $book){
        $bookArray[$i] = $book;

        $bookArray[$i]['image_url'] = bookImage($book['image']);

        // isbn
        $this->db->select('name, value');
        $isbnRes = $this->db->get_where('book_isbn',array('site_id'=>$this->config->item('site_id'), 'book_id'=>$book['id']));
        $bookArray[$i]['isbnlist'] = $isbnRes->result_array();

        // course
        $this->db->select('code');
        $courseRes = $this->db->get_where('tue_book_course',array('book_id'=>$book['id']));
        $bookArray[$i]['courselist'] = $courseRes->result_array();

        // number of advertisements
        $this->db->select('id');
        $this->db->where_in('status_id', array(1,3));
        $adRes = $this->db->get_where('advertisement',array('site_id'=>$this->config->item('site_id'), 'book_id'=>$book['id'], 'isforsale'=>1));
        $bookArray[$i]['offered'] = $adRes->num_rows();

        // number of gezocht
        $this->db->select('id');
        $this->db->where_in('status_id', array(1,3));
        $adRes = $this->db->get_where('advertisement',array('site_id'=>$this->config->item('site_id'), 'book_id'=>$book['id'], 'isforsale'=>0));
        $bookArray[$i]['requested'] = $adRes->num_rows();

        // url / hyperlink
        $bookArray[$i]['url'] = base_url('book/'.$book['slug']);

        // url / hyperlink
        $bookArray[$i]['description'] = strip_tags(html_entity_decode($bookArray[$i]['description']));
        $bookArray[$i]['shortdescription'] = word_limiter(strip_tags(html_entity_decode($bookArray[$i]['description'])),20);



        $i++;
      }

      if($limit == 1){
        $bookArray = $bookArray[0];
      }

      return $bookArray;

    } else {
      return false;
    }
    return false;
  }
}
?>