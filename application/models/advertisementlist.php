<?php
Class Advertisementlist extends CI_Model
{

  public $id;
  public $bookArray;

  function __construct(){
    parent::__construct();
    $this->lang->load('book_status');
    $this->lang->load('book_state');
    $this->load->helper('text');
  }

  function get($where = array(), $status_id = array(1), $order, $limit = ''){
    $this->db->select('id, book_id, user_id, isforsale, price, status_id, state_id, description, created, lastchange');
    
    $this->db->where_in('status_id', $status_id);

    if($limit != '')
      $this->db->limit($limit);
    
    $this->db->order_by($order);

    $adRes = $this->db->get_where('advertisement',$where);

    $adArray = array();
    $i = 0;

    if($adRes->num_rows() > 0){

      foreach($adRes->result_array() as $ad){

        // get book
        $bookRes = $this->db->get_where('book', array('id'=>$ad['book_id']));

        if($bookRes->num_rows() != 0){

          // general array
          $adArray[$i] = $ad;
          $adArray[$i]['book'] = $bookRes->row_array();

          // short description
          $adArray[$i]['book']['shortdescription'] = word_limiter($adArray[$i]['book']['description'],30);

          // price
          setlocale(LC_MONETARY, 'en_US');
          $adArray[$i]['price'] = money_format('%i', $adArray[$i]['price']);


          // get user
          $this->db->select('name');
          $userRes = $this->db->get_where('user',array('id'=>$ad['user_id']));
          $user = $userRes->row_array();
          $adArray[$i]['name'] = $user['name'];

          // get state
          $this->db->select('languagekey');
          $stateRes = $this->db->get_where('state',array('id'=>$ad['state_id']));
          $state = $stateRes->row_array();
          $adArray[$i]['state'] = lang($state['languagekey']);

          // ad url
          $adArray[$i]['url'] = base_url('book/'.$adArray[$i]['book']['slug'].'/'.$ad['id']);

          $i++;

        }
      }

      if($i > 0)
        return $adArray;
      else
        return false;

    } else {
      return false;
    }
  }
}
?>