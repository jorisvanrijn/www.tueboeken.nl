<?php

//placeadvertisement_success.php
$lang['place_ad_success'] = 'Je advertentie is <strong>toegevoegd</strong>!';
$lang['place_ad_success_msg_before'] = 'De advertentie voor het boek "';
$lang['place_ad_success_msg_after'] = '" is toegevoegd. :-)';
$lang['place_ad_success_link'] = 'Klik hier om het te bekijken.';
$lang['place_ad_success_page_title'] = 'Succesvol toegevoegd!';
$lang['place_ad_success_tip'] = '<strong>Tip:</strong> deel het bericht op social media om het bereik te vergroten!';
?>