<?php

//ad
$lang['author'] = 'Auteur';
$lang['used_book_by'] = 'Gebruikt boek door';
$lang['free_shipping'] = 'Gratis verzending door';
$lang['buy_new'] = 'Koop nieuw';
$lang['sorry'] = 'Het spijt ons!';
$lang['hmm'] = 'Hmm..';
$lang['whoa'] = 'Oeps!';

$lang['no_offers'] = 'Geen advertenties beschikbaar voor';
$lang['no_requests'] = 'Geen gezochte advertenties beschikbaar voor';
$lang['no_both'] = 'No aanbiedingen & gezochte advertenties beschikbaar voor';

$lang['course_codes'] = 'Vak codes';

//ad delete
$lang['ad_del_text_1'] = 'Waarom wil je de advertentie verwijderen?';
$lang['ad_del_text_2'] = 'Dit kan niet met Ctrl+Z worden opgelost!';
$lang['ad_del_button_1'] = 'Ik heb het verkocht';
$lang['ad_del_button_2'] = 'Gelijk verwijderen';
$lang['ad_del_button_3'] = 'Houd het boek';

//ad offer

//ad offer sent
$lang['ad_sent_thanks_1'] = 'Bedankt voor je <strong>reactie</strong>';
$lang['ad_sent_thanks_2'] = 'Je bericht is naar de aanbieder verstuurd. In jouw inbox heb je een kopie van het bericht ontvangen.';

//offers n requests
$lang['offers'] = 'Aanbiedingen';
$lang['requests'] = 'Gezochte advertenties';
$lang['all_requests'] = 'All gezochte advertenties';
$lang['buynsell'] = 'Koop & Verkoop';
$lang['all_offers'] = 'Alle aanbiedingen';
$lang['offers_past_tence'] = 'Aangeboden';
$lang['requests_past_tence'] = 'Gezocht';
$lang['requests_action'] = 'Verkoop dit book';
$lang['offers_action'] = 'Koop dit book';

//search
$lang['starting_with_not_found'] = 'We hebben geen boeken gevonden die beginnen met de';

//me
$lang['me'] = 'ik';

//about
$lang['about_title_1'] = 'Universitaire Boeken';
$lang['about_text_1'] = 'Universitaire Boeken is een initiatief van 2 studenten (Bas Schleijpen and Joris van Rijn) die zich realiseren dat boeken een van de meest vervelende dingen is
		voor de bankrekening van een student. Door een landelijk netwerk op te zetten hopen ze zo de schade beperkt te houden in deze tijden van 
		<a href="http://www.rijksoverheid.nl/onderwerpen/studiefinanciering/kabinetsplannen-studiefinanciering-studievoorschot" target="_blank">slechte beslisingen</a>.';

$lang['about_title_2'] = 'LeidenUniv Boeken';
$lang['about_text_2'] = '<a href="http://www.leidenunivboeken.nl">LeidenUniv Boeken</a> is met de hulp van een Leidse student (Friso vd Pol) opgezet na het succes in Leiden. LeidenUniv boeken is de 2e website in de landelijke Universitaire Boeken sites serie.';

$lang['about_title_3'] = 'Sponsors & Partners';
$lang['about_text_3'] = 'We staan open voor advertenties op onze website. We bieden ruimte op de pagina\'s die per jaar 20.000 door de top studenten van Leiden gezien worden. Neem <a href="'.site_url('contact').'">contact</a> op voor meer details.';

//footer
$lang['additional_links'] = 'Overige links';
$lang['termsnconditions'] = 'Algemene voorwaarden';
$lang['about'] = 'Over';
$lang['mailing_adress'] = 'Postadres';
$lang['visitor_adress'] = 'Bezoekadres';
$lang['v_adress'] = 'Horsten 1<br/>5612AX, Eindhoven<br/>Multimedia Paviljoen, TU/e';
$lang['by_appointment'] = '(alleen op afspraak)';
$lang['part_of'] = 'LeidenUniv Boeken is onderdeel van';
$lang['no_partner'] = 'LeidenUniv boeken is <u>geen</u> offici&euml;le partner van de Universiteit Leiden';

// settings
$lang['settings_contact_email'] = 'contact@tueboeken.nl';
$lang['settings_contact_from'] = 'jouw@email.nl';
$lang['settings_contact_from_name'] = 'No Reply';

//contact
$lang['title_site_name_plain'] = 'LeidenUniv Boeken';

//404
$lang['404_line_1'] = 'Zucht..! Het is een vier-nul-vier';
$lang['404_line_2'] = 'Het lijkt er op dat we faalden - sorry!';
$lang['404_line_3'] = 'De opgevraagde pagina is niet beschikaar. Gebruik het menu of de zoekbalk hierboven op zo snel mogelijk hier weg te gaan.';

//home
$lang['home_lang_subtitle_2'] = 'boeken te <strong>kopen & verkopen</strong>';
$lang['all_books'] = 'Bekijk alle boeken &raquo;';

//login and register
$lang['register_panel_title'] = 'Begin met <strong>kopen & verkopen</strong>';
$lang['login_panel_title'] = 'Login';
$lang['login_panel_title_more'] = 'Login en begin met <strong>kopen & verkopen</strong>';
$lang['forgot_pass_panel_title'] = '<strong>Wachtwoord</strong> vergeten?';
$lang['forgot_pass_panel_text'] = 'Vul hier je email in om een nieuw wachtwoord te ontvangen.';
$lang['forgot_pass_panel_link'] = 'Weet je opeens je wachtwoord weer?';

//add book
$lang['add_step_1'] = 'Stap 1) Selecteer een <strong>boek</strong>';
$lang['add_step_2'] = 'Stap 2) Advertentie <strong>details</strong>';
$lang['add_step_3'] = 'Stap 3) Jouw <strong>gegevens</strong>';
$lang['prev'] = '&laquo; Vorige';
$lang['next'] = 'Volgende &raquo;';

//search
$lang['search_button'] = 'Zoeken';
$lang['search_text'] = 'Zoek op vakcode, titel, auteur, ISBN, etc.';

$lang['search_add_found_1'] = 'We vonden';
$lang['search_add_found_2'] = 'advertentie(s) - Kan je jouw boek niet vinden?';
$lang['search_add_found_3'] = 'Voeg het nu toe';

$lang['not_found_question_1'] = 'Geen boeken gevonden';
$lang['not_found_question_2'] = 'Is het boek nog niet te vinden in onze database?';
$lang['not_found_question_3'] = 'Voeg handmatig een boek toe';

//profile
$lang['edit_profile'] = 'Bewerk mijn profiel';
$lang['no_desc'] = 'Geen beschrijving opgegeven';
$lang['change_this'] = 'Verander dit';
$lang['found_sold'] = 'Books gekocht/verkocht';
$lang['my_ads'] = 'Mijn advertenties';
$lang['other_ads'] = 'advertenties';
$lang['profile_latest_replies'] = 'Laatste reacties';

$lang['profile_books'] = 'boeken';
$lang['profile_book'] = 'Boek';
$lang['profile_type'] = 'Type';
$lang['profile_since'] = 'Sinds';
$lang['profile_actions'] = 'Acties';
$lang['profile_edit'] = 'Bewerk';
$lang['profile_delete'] = 'Verwijder';
$lang['profile_message'] = 'Bericht';
$lang['profile_info'] = 'Info';
$lang['profile_mail'] = 'Mail';

$lang['no_books_profile_1'] = 'Het lijkt er op dat je nog geen boeken hebt toegevoegd';
$lang['no_books_profile_2'] = 'Klik hier';
$lang['no_books_profile_3'] = 'om te beginnen!';

$lang['no_books_profile_4'] = 'biedt op het moment geen boeken aan. Wij vinden het ook niet leuk..';

$lang['no_replies_profile'] = 'Je hebt nog geen reacties. Heb je wel boeken toegevoegd?';

//profile delete
$lang['delprofile_line_1'] = 'Weet je zeker dat je jouw profiel wil verwijderen?';
$lang['delprofile_line_2'] = 'Dit is niet te oplossen met Ctrl+Z!';
$lang['delprofile_button_1'] = 'Nee';
$lang['delprofile_button_2'] = 'Ja';

//profile edit
$lang['editprofile_old'] = 'Oud';
$lang['editprofile_new'] = 'Nieuw';
$lang['editprofile_repeat'] = 'Herhaal';
$lang['editprofile_savepass'] = 'Sla wachtwoord op';

$lang['editprofile_otheractions'] = 'Andere acties';
$lang['editprofile_deleteprofile'] = 'Profiel verwijderen';

$lang['search_filter_title']= 'Filters';
$lang['search_searches_found']= 'Resultaten voor:';

$lang['search_advertisements']= 'advertentie(s)';

$lang['By course code']= 'Op vak code';

//sitemap
$lang['sitemap_link_title']= 'Sitemap';
$lang['sitemap_link_1']= 'Home';
$lang['sitemap_link_2']= 'Boeken A-Z';
$lang['sitemap_link_3']= 'Login';
$lang['sitemap_link_4']= 'Registreer';
$lang['sitemap_link_5']= 'Kopen & Verkopen';
$lang['sitemap_link_6']= 'Profiel';
$lang['sitemap_link_7']= 'Profiel bewerken';
$lang['sitemap_link_8']= 'Algemene voorwaarden';
$lang['sitemap_link_9']= 'Over';
$lang['sitemap_link_10']= 'Sitemap';
$lang['sitemap_link_11']= 'Contact';

$lang['termsnconditions_lastupdate']= 'Laatste update: 17-1-2015';
$lang['termsnconditions_download']= 'Download algemene voorwaarden';

//main navigation
$lang['main_nav_home']= 'Home';
$lang['main_nav_buynsell']= 'Kopen & Verkopen';
$lang['main_nav_books']= 'Boeken A-Z';
$lang['main_nav_logout']= 'Uitloggen';
$lang['main_nav_register']= 'Registreer';

$lang['profile_box_register'] = 'Registreer';
$lang['profile_box_logout'] = 'Uitloggen';