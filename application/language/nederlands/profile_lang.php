<?php

	// Login
	$lang['profile_view_title'] = 'Mijn profiel';
	$lang['profile_ads_title'] = 'Mijn advertenties';
	$lang['profile_change_title'] = 'Profiel bewerken';

	// Change profile texts
	$lang['password_change_title'] = 'Verander wachtwoord';
	$lang['info_change_title'] = 'Verander informatie';
	$lang['info_change_startyear'] = 'Begin jaar';
	$lang['info_change_phone'] = 'Telefoon nummer';
	$lang['info_change_phone_placeholder'] = '06 00000000';
	$lang['info_change_description'] = 'Profiel beschrijving';
	$lang['info_change_description_placeholder'] = 'Schrijf hier iets op waardoor mensen je vertrouwen';
	$lang['info_change_submit'] = 'Sla informatie op';

	// Change profile alerts
	$lang['profile_changed_title'] = 'Profiel bewerkt';
	$lang['profile_changed_message'] = 'Je profiel is succesvol opgeslagen';
	$lang['profile_not_changed_title'] = 'Profiel niet bewerkt';
	$lang['profile_not_changed_message'] = 'Je profiel is helaas niet bewerkt';

	// Change Password alerts
	$lang['password_changed_title'] = 'Wachtwoord is veranderd';
	$lang['password_changed_message'] = 'Je wachtwoord is succesvol opgeslagen';
	$lang['password_not_changed_title'] = 'Wachtwoord is niet veranderd';
	$lang['password_not_changed_message'] = 'Je wachtwoord kon helaas niet opgeslagen worden';
	$lang['password_not_changed_message_old_password'] = 'Het oude wachtwoord is incorrect';
	$lang['password_not_changed_message_new_password'] = 'Het eerste en tweede wachtwoord komen niet overeen';

?>