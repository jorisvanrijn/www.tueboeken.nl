<?php

// Login
$lang['home_contact_title'] = 'Contact';
$lang['home_about_title'] = 'Over';
$lang['home_login_title'] = 'Login';
$lang['home_register_title'] = 'Registreer';
$lang['home_login_noaccount'] = 'Nog geen account?';
$lang['home_login_noaccountsub'] = 'Registreer gratis om nu te verkopen en kopen!';
$lang['home_login_name'] = 'Naam';
$lang['home_login_nameplaceholder'] = 'Volledige naam';
$lang['home_login_study'] = 'Studie';
$lang['home_login_email'] = 'Email';
$lang['home_login_emailplaceholder'] = 'voorbeeld@umail.leidenuniv.nl';
$lang['home_login_emailsub'] = '';
$lang['home_login_password'] = 'Wachtwoord';
$lang['home_login_passwordplaceholder'] = 'Kies een wachtwoord';
$lang['home_login_repeat'] = 'Herhaal';
$lang['home_login_repeatplaceholder'] = 'Herhaal het wachtwoord';
$lang['home_login_submit'] = 'Registreer nu';
$lang['home_login_login'] = 'of gelijk inloggen';
$lang['home_login_forgot'] = 'Wachtwoord vergeten?';
$lang['home_login_error'] = 'Ongeldig email en/of wachtwoord';
$lang['home_login_email_exists'] = 'Het opgegeven email adres bestaal al! Maximaal &eacute;&eacute;n account per gebruiker toegestaan.';

// welcome e-mail, [name] is replaced by name, [email] by e-mail, [password] by the password.
$lang['home_register_email_subject'] = 'Geregistreerd op leidenunivboeken.nl';
$lang['home_register_email_message'] = 'Beste [name],

Bedankt voor het registeren op leidenunivboeken.nl!';

// forgot password e-mail, [name] is replaced by name, [email] by e-mail, [password] by the password.
$lang['home_forgot_title'] = 'Wachtwoord vergeten';
$lang['home_forgot_email_subject'] = 'LeidenUniv Boeken wachtwoord resetten';
$lang['home_forgot_email_message'] = 'Beste [name],


Je hebt een wachtwoord reset opgevraagd voor je LeidenUniv Boeken account.

We hebben het volgende wachtwoord aangemaakt:
[password]

Hoewel het niet verplicht is raden we je sterk aan om het wachtwoord na inloggen te veranderen.

Groetjes,

TU/e boeken team';
$lang['home_forgot_enter'] = 'Vul een emailadres in';

$lang['home_lang_subtitle'] = 'Een <strong>gratis</strong> marktplaats om';

?>