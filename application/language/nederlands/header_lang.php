<?php

// $lang['language_key'] = 'text....';

$lang['site_name'] = 'LeidenUniv Boeken';
$lang['site_title'] = 'Platform voor tweedehands studieboeken in Leiden';
$lang['title_seperator'] = ' - ';
$lang['header_link_1'] = 'Advertenties';
$lang['header_link_2'] = 'Profiel';

$lang['profile_box_profile'] = 'Profiel';
$lang['profile_box_login'] = 'Login';
$lang['profile_box_or'] = 'of';
$lang['profile_box_title'] = 'Login in a.u.b.';

?>