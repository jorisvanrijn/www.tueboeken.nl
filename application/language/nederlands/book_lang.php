<?php

// titles
$lang['book_overview_title'] = 'Alle boeken';
$lang['book_add_title'] = 'Kopen & Verkopen';

// form
$lang['book_ad_type'] = 'Type advertentie';
$lang['book_ad_type_note'] = 'Type van de advertentie';
$lang['book_ad_type_sell'] = 'Verkopen (aanbieden)';
$lang['book_ad_type_buy'] = 'Kopen (op zoek)';

$lang['book_title'] = 'Boek titel';
$lang['book_title_note'] = 'Titel van het boek';
$lang['book_title_placeholder'] = '';
$lang['book_title_error'] = 'Vul alstublieft een titel van het boek in';

$lang['book_isbn'] = 'ISBN nummer';
$lang['book_isbn_note'] = 'Laat het leeg als er geen ISBN nummer beschikbaar is';
$lang['book_isbn_placeholder'] = 'Bijv. 000-0-000-00000-0';
$lang['book_isbn_error'] = 'Vul een geldig ISBN nummer in (geen spaties)';

$lang['book_state'] = 'Status van het boek';
$lang['book_state_note'] = 'Is het boek nieuw of gebruikt?';

$lang['book_coursecode'] = 'Vak code';
$lang['book_coursecode_note'] = '';
$lang['book_coursecode_placeholder'] = 'Bijv. 0ABC0';
$lang['book_coursecode_dontknow'] = 'Laat het leeg als er geen vakcode bij het boek hoort';

$lang['book_description'] = 'Boek beschrijving';
$lang['book_description_note'] = 'Bijv. status van het boek of andere bijzonderheden';
$lang['book_description_placeholder'] = 'Bijv. status van het boek of andere bijzonderheden';

$lang['book_price'] = 'Book prijs';

// ads
$lang['book_offer_price'] = 'Prijs';
$lang['book_offer_state'] = 'Status';
$lang['book_offer_description'] = 'Beschrijving';

// respond
$lang['book_respond'] = 'Nu reageren';
$lang['book_respond_message'] = 'Bericht';
$lang['book_respond_message_subtitle'] = 'Reageer op het bericht of doe een tegenbod';
$lang['book_respond_message_value'] = 'Beste [seller],

Ik heb interesse in het boek "[title]". Zullen we meeten op de campus in ... zodat ik het boek kan zien?

Groetjes,

[name]';

$lang['book_reply_subject'] = 'Reactie op het boek - LeidenUnivBoeken.nl';
$lang['book_reply_message'] = 'Hey [name],

Je hebt een reactie op het boek: "[title]".
Advertentie: [url]

================
Naam: [buyer]
Email: [email]
Telefoonnummer: [phone]
Bericht:
[message]
================

Groetjes,

TU/e boeken team';

?>