<?php

$lang['book_state_new'] = 'Out of the box';
$lang['book_state_good'] = 'Good - almost new';
$lang['book_state_used'] = 'Used but okay';
$lang['book_state_bad'] = 'Not great';
$lang['book_state_read'] = 'Read once or twice';

?>