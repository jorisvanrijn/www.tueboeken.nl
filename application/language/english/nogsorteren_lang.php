<?php

//ad
$lang['author'] = 'Author';
$lang['used_book_by'] = 'Used book by';
$lang['free_shipping'] = 'Free shipping by';
$lang['buy_new'] = 'Buy new';
$lang['sorry'] = 'We\'re sorry!';
$lang['hmm'] = 'Hmm..';
$lang['whoa'] = 'Whoa!';

$lang['no_offers'] = 'No offers are available for';
$lang['no_requests'] = 'No requests are available for';
$lang['no_both'] = 'No offers & requests are available for';

$lang['course_codes'] = 'Course codes';

//ad delete
$lang['ad_del_text_1'] = 'Why do you want to delete this ad?';
$lang['ad_del_text_2'] = 'There is no Ctrl+Z after this!';
$lang['ad_del_button_1'] = 'I sold it';
$lang['ad_del_button_2'] = 'Just delete it';
$lang['ad_del_button_3'] = 'Nevermind, keep it';

//ad offer

//ad offer sent
$lang['ad_sent_thanks_1'] = 'Thanks for your <strong>reply</strong>';
$lang['ad_sent_thanks_2'] = 'Your email had been succesfully sent to the advertiser. You will receive a copy of it.';

//offers n requests
$lang['requests'] = 'Requests';
$lang['offers'] = 'Offers';
$lang['all_requests'] = 'All requests';
$lang['buynsell'] = 'Buy & Sell';
$lang['all_offers'] = 'All offers';
$lang['offers_past_tence'] = 'Offered';
$lang['requests_past_tence'] = 'Requested';
$lang['requests_action'] = 'Request this book';
$lang['offers_action'] = 'Offer this book';

//search
$lang['starting_with_not_found'] = 'We couldn\'t find books starting with a';

//me
$lang['me'] = 'me';

//about
$lang['about_title_1'] = 'Universitaire Boeken';
$lang['about_text_1'] = 'Universitaire Boeken is an initiative of three friends (Bas Schleijpen, Joris van Rijn and Friso van de Pol (who’s joined later on)), who realised that studybooks were fiercely attacking students’ cash. By enrolling a nationwide network of decentralized sites, we hope to minimize the yearly damage. </p><p>
The advantages of decentralized sites are massive. All issues regarding the shipment of books dissapear because everyone is enrolled at the same university, making the selling/buying of books cheaper, more reliable and easier. Also, everyone uses the same books! Winning!</p><p>
And the best is yet to come: the site is free and will be so till eternity! We won’t spam you with ads either: all ads will be useful for you! Think about info about potential employers, or nice deals at (local) companies!</p><p>
At this moment the network consists of TU Eindhoven (<a href="http://www.tueboeken.nl">www.tueboeken.nl</a>) and Universiteit Leiden (<a href="http://www.leidenunivboeken.nl">www.leidenunivboeken.nl</a>). ';

$lang['about_title_2'] = 'TU/e Boeken';
$lang['about_text_2'] = 'This version of tueboeken.nl was made with lot\'s of love in Eindhoven. Together with a team of 3 (Ranice Janssen, Ileen Smits and Laura Bosdriesz) the complete user interaction and design was renewed. Many thanks to them for helping in realising the new TU/e boeken! </p><p>

Fancy a cup of tea? For comments regarding TU/e boeken <a href="'.site_url('contact').'">contact</a> Joris!
';

$lang['about_title_3'] = 'Sponsors & Partnerships: TU/e boeken';
$lang['about_text_3'] = 'Wij staan open voor samenwerking. Wilt u jaarlijks ook meer dan 10.000 views op uw advertenties in Leiden, of nog veel meer via het landelijke netwerk? Geef ons eens een <a href="'.site_url('contact').'">belletje of stuur een mail</a>! Wij lichten graag alles uitgebreid toe, en zijn zeer flexibel in de mogelijkheden.</p><p> 

We are willing to join forces. Interested in more than 10.000 views a year on TU/e boeken or even more through the national network? Give us a <a href="'.site_url('contact').'">call or send e-mail</a>! We happily provide for extra information, and we are extremely flexible regarding possibilities.';

//footer
$lang['additional_links'] = 'Additional links';
$lang['termsnconditions'] = 'Terms & Conditions';
$lang['about'] = 'About';
$lang['mailing_adress'] = 'Mailing Adress';
$lang['visitor_adress'] = 'Visitor adress';
$lang['v_adress'] = 'Horsten 1<br/>5612AX, Eindhoven<br/>Multimedia Paviljoen, TU/e';
$lang['by_appointment'] = '(only by appointment)';
$lang['part_of'] = 'TU/e boeken is part of';
$lang['no_partner'] = 'TU/e boeken is <u>not</u> an official partner of the TU/e';

// settings
$lang['settings_contact_email'] = 'contact@tueboeken.nl';
$lang['settings_contact_from'] = 'system@tueboeken.nl';
$lang['settings_contact_from_name'] = 'No Reply';

//contact
$lang['title_site_name_plain'] = 'TU/e Boeken';

//404
$lang['404_line_1'] = 'Shoot! It\'s a four-oh-four';
$lang['404_line_2'] = 'Seems we\'ve failed, excuse and forgive us.';
$lang['404_line_3'] = 'The requested page was not found. Use the search bar above or the navigation to leave this awful page.';

//home
$lang['home_lang_subtitle_2'] = '<strong>sell</strong> & <strong>buy</strong> books to & from fellow students';
$lang['all_books'] = 'View all books &raquo;';

//login and register
$lang['register_panel_title'] = 'Start <strong>selling & buying</strong>';
$lang['login_panel_title'] = 'Login';
$lang['login_panel_title_more'] = 'Login to start <strong>selling & buying</strong>';
$lang['forgot_pass_panel_title'] = 'Forgot your <strong>password?</strong>';
$lang['forgot_pass_panel_text'] = 'Enter your email address to get a new password.';
$lang['forgot_pass_panel_link'] = 'Suddenly remembered your password again?';

//add book
$lang['add_step_1'] = 'Step 1) Select a <strong>book</strong>';
$lang['add_step_2'] = 'Step 2) Specify ad <strong>details</strong>';
$lang['add_step_3'] = 'Step 3) <strong>Your details</strong>';
$lang['prev'] = '&laquo; Previous';
$lang['next'] = 'Next &raquo;';

//search
$lang['search_button'] = 'Search';
$lang['search_text'] = 'Search for a TU/e subject code, book title, author, ISBN, etc.';

$lang['search_add_found_1'] = 'We\'ve found';
$lang['search_add_found_2'] = 'advertisement(s) - Is your book not in this list?';
$lang['search_add_found_3'] = 'Add it yourself';

$lang['not_found_question_1'] = 'No books were found.';
$lang['not_found_question_2'] = 'Isn\'t your book already in our database?';
$lang['not_found_question_3'] = 'Add book manually';

//profile
$lang['edit_profile'] = 'Edit my profile';
$lang['no_desc'] = 'No description given.';
$lang['change_this'] = 'Change this';
$lang['found_sold'] = 'Books found/sold';
$lang['my_ads'] = 'My ads';
$lang['other_ads'] = 'ads';
$lang['profile_latest_replies'] = 'Latest Replies';

$lang['profile_books'] = 'books';
$lang['profile_book'] = 'Book';
$lang['profile_type'] = 'Type';
$lang['profile_since'] = 'Since';
$lang['profile_actions'] = 'Actions';
$lang['profile_edit'] = 'Edit';
$lang['profile_delete'] = 'Delete';
$lang['profile_message'] = 'Message';
$lang['profile_info'] = 'Info';
$lang['profile_mail'] = 'Mail';

$lang['no_books_profile_1'] = 'Seems like you didnt\'t add any books.';
$lang['no_books_profile_2'] = 'Click here';
$lang['no_books_profile_3'] = 'to start!';

$lang['no_books_profile_4'] = 'currently doesn\'t offer any books. We dont\'t like it either..';

$lang['no_replies_profile'] = 'Seems like no seems to notice you. Did you add any books?';

//profile delete
$lang['delprofile_line_1'] = 'Are you sure you want to delete your profile?';
$lang['delprofile_line_2'] = 'There is no Ctrl+Z after this!';
$lang['delprofile_button_1'] = 'No';
$lang['delprofile_button_2'] = 'Yes';

//profile edit
$lang['editprofile_old'] = 'Old';
$lang['editprofile_new'] = 'New';
$lang['editprofile_repeat'] = 'Repeat';
$lang['editprofile_savepass'] = 'Save password';

$lang['editprofile_otheractions'] = 'Other actions';
$lang['editprofile_deleteprofile'] = 'Delete profile';

$lang['search_filter_title']= 'Filters';
$lang['search_searches_found']= 'Results for:';

$lang['search_advertisements']= 'advertisement(s)';

$lang['By course code']= 'By course code';

//sitemap
$lang['sitemap_link_title']= 'Sitemap';
$lang['sitemap_link_1']= 'Home';
$lang['sitemap_link_2']= 'Books A-Z';
$lang['sitemap_link_3']= 'Login';
$lang['sitemap_link_4']= 'Register';
$lang['sitemap_link_5']= 'Buy & Sell';
$lang['sitemap_link_6']= 'Profile';
$lang['sitemap_link_7']= 'Edit profile';
$lang['sitemap_link_8']= 'Terms & Conditions';
$lang['sitemap_link_9']= 'About';
$lang['sitemap_link_10']= 'Sitemap';
$lang['sitemap_link_11']= 'Contact';

$lang['termsnconditions_lastupdate']= 'Last update: 17-1-2015';
$lang['termsnconditions_download']= 'Download terms & conditions (Dutch)';

//main navigation
$lang['main_nav_home']= 'Home';
$lang['main_nav_buynsell']= 'Buy & Sell';
$lang['main_nav_books']= 'Books A-Z';
$lang['main_nav_logout']= 'Logout';
$lang['main_nav_register']= 'Register';

$lang['profile_box_register'] = 'Register';
$lang['profile_box_logout'] = 'Logout';