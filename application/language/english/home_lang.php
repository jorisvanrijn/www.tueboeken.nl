<?php

// Login
$lang['home_contact_title'] = 'Contact';
$lang['home_about_title'] = 'About';
$lang['home_login_title'] = 'Login';
$lang['home_register_title'] = 'Register';
$lang['home_login_noaccount'] = 'Not registered yet?';
$lang['home_login_noaccountsub'] = 'Register for free to sell and buy used books!';
$lang['home_login_name'] = 'Name';
$lang['home_login_nameplaceholder'] = 'Full name';
$lang['home_login_study'] = 'Study';
$lang['home_login_email'] = 'Email';
$lang['home_login_emailplaceholder'] = 'example@student.tue.nl';
$lang['home_login_emailsub'] = '';
$lang['home_login_password'] = 'Password';
$lang['home_login_passwordplaceholder'] = 'Choose a password';
$lang['home_login_repeat'] = 'Repeat';
$lang['home_login_repeatplaceholder'] = 'Repeat the password';
$lang['home_login_submit'] = 'Register now';
$lang['home_login_login'] = 'Or login directly';
$lang['home_login_forgot'] = 'Forgot your password?';
$lang['home_login_error'] = 'Invalid email or password';
$lang['home_login_email_exists'] = 'Email already exists. There is only one account per email allowed.';

// welcome e-mail, [name] is replaced by name, [email] by e-mail, [password] by the password.
$lang['home_register_email_subject'] = 'Registered at tueboeken.nl';
$lang['home_register_email_message'] = 'Hi [name],

Thanks for registering on TUEboeken.nl!';

// forgot password e-mail, [name] is replaced by name, [email] by e-mail, [password] by the password.
$lang['home_forgot_title'] = 'Forgot password';
$lang['home_forgot_email_subject'] = 'TUEboeken.nl password reset';
$lang['home_forgot_email_message'] = 'Hi [name],


You requested a password reset for your account.

We created a new password for you:
[password]

Even though it is not mandatory, we strongly recommend you to change it as soon as you login.

Regards,

TU/e boeken team';
$lang['home_forgot_enter'] = 'Please enter e email address.';

$lang['home_lang_subtitle'] = 'A completely <strong>free</strong> marketplace to';

?>