<?php

//placeadvertisement_success.php
$lang['place_ad_success'] = 'Your ad has been <strong>successfully</strong> added!';
$lang['place_ad_success_msg_before'] = 'Your ad for the book "';
$lang['place_ad_success_msg_after'] = '" has been added. :-)';
$lang['place_ad_success_link'] = 'Click here to view it.';
$lang['place_ad_success_page_title'] = 'Ad successfully added!';
$lang['place_ad_success_tip'] = '<strong>Tip:</strong> share the following link on social media to tell your friends you sell the book.';
?>