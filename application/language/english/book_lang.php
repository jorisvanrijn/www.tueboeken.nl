<?php

// titles
$lang['book_overview_title'] = 'All books';
$lang['book_add_title'] = 'Buy & Sell';

// form
$lang['book_ad_type'] = 'Type of advertisement';
$lang['book_ad_type_note'] = 'Type of advertisement';
$lang['book_ad_type_sell'] = 'Sell (offer)';
$lang['book_ad_type_buy'] = 'Buy (request)';

$lang['book_title'] = 'Book title';
$lang['book_title_note'] = 'Title of the book';
$lang['book_title_placeholder'] = '';
$lang['book_title_error'] = 'Please fill in the title of the book.';

$lang['book_isbn'] = 'ISBN number';
$lang['book_isbn_note'] = 'Leave it blank if there is no ISBN number';
$lang['book_isbn_placeholder'] = 'E.g. 000-0-000-00000-0';
$lang['book_isbn_error'] = 'Please fill in a valid ISBN number. Do not use spaces.';

$lang['book_state'] = 'State of the book';
$lang['book_state_note'] = 'Is it used a lot or almost new?';

$lang['book_coursecode'] = 'TU/e course code';
$lang['book_coursecode_note'] = '';
$lang['book_coursecode_placeholder'] = 'E.g. 0ABC0';
$lang['book_coursecode_dontknow'] = 'Leave it blank if there are no course codes.';

$lang['book_description'] = 'Book description';
$lang['book_description_note'] = 'E.g. details about the state of the book.';
$lang['book_description_placeholder'] = 'E.g. details about the state of the book.';

$lang['book_price'] = 'Book price';

// ads
$lang['book_offer_price'] = 'Price';
$lang['book_offer_state'] = 'State';
$lang['book_offer_description'] = 'Description';

// respond
$lang['book_respond'] = 'Respond now';
$lang['book_respond_message'] = 'Message';
$lang['book_respond_message_subtitle'] = 'E.g. reply to the ad or make an offer.';
$lang['book_respond_message_value'] = 'Dear [seller],

I am interesed in your book "[title]". Shall we meet in the ... at the TU/e, so I can see the book?

Regards,

[name]';

$lang['book_reply_subject'] = 'You received an offer for your book - TUEboeken.nl';
$lang['book_reply_message'] = 'Hi [name],

You received an offer for your book "[title]".
Advertisement: [url]

================
Name: [buyer]
Email: [email]
Phone: [phone]
Message:
[message]
================

Regards,

TU/e boeken team';

$lang['book_reply_c_subject'] = 'You sent a offer - TUEboeken.nl';
$lang['book_reply_c_message'] = 'Hi [buyer],

Here is a copy of the offer you sent to [name] for the book "[title]".
Advertisement: [url]

================
Name: [buyer]
Email: [email]
Phone: [phone]
Message:
[message]
================

Regards,

TU/e boeken team';

?>