<?php

	// Login
	$lang['profile_view_title'] = 'My profile';
	$lang['profile_ads_title'] = 'My advertisements';
	$lang['profile_change_title'] = 'Change profile';

	// Change profile texts
	$lang['password_change_title'] = 'Change password';
	$lang['info_change_title'] = 'Change information';
	$lang['info_change_startyear'] = 'Start year';
	$lang['info_change_phone'] = 'Phone number';
	$lang['info_change_phone_placeholder'] = '+316 00000000';
	$lang['info_change_description'] = 'Profile description';
	$lang['info_change_description_placeholder'] = 'A general description for your profile (where all your advertisements are shown). Tip: tell why you sell your books and how people can contact you.';
	$lang['info_change_submit'] = 'Save information';

	// Change profile alerts
	$lang['profile_changed_title'] = 'Profile updated';
	$lang['profile_changed_message'] = 'Your profile has been succesfully updated';
	$lang['profile_not_changed_title'] = 'Profile update failed';
	$lang['profile_not_changed_message'] = 'Your profile has not been updated';

	// Change Password alerts
	$lang['password_changed_title'] = 'Password updated';
	$lang['password_changed_message'] = 'Your password has been succesfully updated';
	$lang['password_not_changed_title'] = 'Password update failed';
	$lang['password_not_changed_message'] = 'Your password has not been updated';
	$lang['password_not_changed_message_old_password'] = 'The old password you entered is incorrect';
	$lang['password_not_changed_message_new_password'] = 'The new passwords do not correspond';

?>