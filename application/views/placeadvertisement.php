<?php echo form_open('addbook/add/'.$bookId.'/',array('class'=>'form-horizontal')); ?>


    <?php if($step == 2) { ?>


        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="panel panel-default step" id="step2" style="display: block;">
                    <div class="panel-heading"><h4 style="margin: 0;"><?php echo lang('add_step_2'); ?></h4>
                    </div>
                    <div class="panel-body">

                            <div class="row">
                                <div class="col-sm-4"><strong><?php echo lang('book_ad_type'); ?></strong>
                                    <br><em style="font-size: 11px;" class="text-muted"><?php echo lang('book_ad_type_note'); ?></em>
                                </div>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select class="form-control" name="adtype">
                                            <option value="1"<?php if(isset($_GET['offer'])) echo ' selected="selected"'; ?>><?php echo lang('book_ad_type_sell'); ?>
                                            <option value="0"<?php if(isset($_GET['request']) or (set_value('adtype') == '0')) echo ' selected="selected"'; ?>><?php echo lang('book_ad_type_buy'); ?>
                                        </select>
                                    </div>
                                    <?php echo form_error('adtype','<div class="alert alert-danger" style="margin-top: 10px; padding-top: 5px; padding-bottom: 5px;">','</div>'); ?>
                                    </span>
                                </div>
                            </div>

                            <hr/>

                            <div class="row">
                                <div class="col-sm-4"><strong><?php echo lang('book_price'); ?></strong>
                                </div>
                                <div class="col-sm-8">
                                    <div class="input-group" style="width:200px;">
                                        <span class="input-group-addon">&euro;</span>
                                        <input type="number" min="0.01" step="0.01" max="300.00" value="<?php echo set_value('price'); ?>" autofocus="" class="form-control ad_price" name="price" placeholder="00.00">
                                    </div>

                                    <!-- <span class="newprice text-muted">Laagste Bol.com prijs: <span>Onbekend</span>
                                    <p></p> -->

                                    <?php echo form_error('price','<div class="alert alert-danger" style="margin-top: 10px; padding-top: 5px; padding-bottom: 5px;">','</div>'); ?>
                                    </span>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-sm-4">
                                    <strong><?php echo lang('book_state'); ?></strong>
                                    <br><em style="font-size: 11px;" class="text-muted"><?php echo lang('book_state_note'); ?></em>
                                </div>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <select name="state" style="min-width:200px;" class="advselect form-control" id="a28906831" tabindex="-1">
                                            <?php

                                                foreach($status as $k => $v){
                                                    echo '<option value="'.$k.'">'.$v.'</option>';
                                                }

                                            ?>
                                        </select>

                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-sm-4">
                                    <strong><?php echo lang('book_coursecode'); ?></strong>
                                    <br><em style="font-size: 11px;" class="text-muted"><?php echo lang('book_coursecode_note'); ?><br><?php echo lang('book_coursecode_placeholder'); ?></em>
                                </div>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input type="text" value="<?php echo set_value('coursecode'); ?>" data-role="tagsinput" class="form-control ad_vakcode" name="coursecode" placeholder="">
                                    </div>
                                    <p><?php echo lang('book_coursecode_dontknow'); ?></p>
                                    <?php echo form_error('coursecode','<div class="alert alert-danger" style="margin-top: 10px; padding-top: 5px; padding-bottom: 5px;">','</div>'); ?>
                                </div>
                            </div>

                        <hr>

                        <div class="row">
                            <div class="col-sm-4"><strong><?php echo lang('book_description'); ?></strong>
                                <br><em style="font-size: 11px;" class="text-muted"><?php echo lang('book_description_note'); ?></em>
                            </div>
                            <div class="col-sm-8">
                                <div class="input-group" style="width: 100%;">
                                    <textarea class="form-control" name="description" style="width: 100%;" rows="3" placeholder="<?php echo lang('book_description_placeholder'); ?>"><?php echo set_value('description'); ?></textarea>
                                </div>

                                <?php echo form_error('description','<div class="alert alert-danger" style="margin-top: 10px; padding-top: 5px; padding-bottom: 5px;">','</div>'); ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php } elseif($step == 3) { ?>

        <input type="hidden" name="step" value="3">

        <input type="hidden" name="adtype" value="<?php echo set_value('adtype'); ?>">
        <input type="hidden" name="price" value="<?php echo set_value('price'); ?>">
        <input type="hidden" name="state" value="<?php echo set_value('state'); ?>">
        <input type="hidden" name="coursecode" value="<?php echo set_value('coursecode'); ?>">
        <input type="hidden" name="description" value="<?php echo set_value('description'); ?>">

        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="panel panel-default step" id="step3">
                    <div class="panel-heading"><h4 style="margin: 0;"><?php echo lang('add_step_3'); ?></h4>
                    </div>
                    <div class="panel-body">
             
                        <div class="row">

                            <div class="col-sm-8">
                                <h3><?php echo lang('home_login_noaccount'); ?></h3>
                                <em><?php echo lang('home_login_noaccountsub'); ?></em>
                                <hr>
                                <?php echo registerForm($showRegisterErrors); ?>
                            </div>


                            <div class="col-sm-4">

                                <div class="panel panel-default" style="margin-top: 20px;">
                                    <div class="panel-heading">
                                        <div class="panel-title"><?php echo lang('home_login_login'); ?></div>
                                    </div>

                                    <div style="padding-top:30px" class="panel-body">
                                    <?php if($page == 'login') { echo validation_errors(); } ?>
                                    
                                    <?php echo loginForm(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 text-right">
            <div class="belowscreen">
                <a href="javascript:history.go(-1);" class="btn btn-primary vorige"><?php echo lang('prev'); ?></a>
                <input type="submit" class="btn btn-primary volgende" value="<?php echo lang('next'); ?>">
            </div>
        </div>
    </div>

</form>