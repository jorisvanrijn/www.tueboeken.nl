<div class="container">
  <div class="row">
  	<div class="col-sm-4 col-sm-offset-4">
		<div class="well well-default text-center">
			<h3 style="margin-top: 0;"><?php echo lang('delprofile_line_1'); ?><br/><strong><?php echo lang('delprofile_line_2'); ?></strong></h3>
			
			<a href="<?php echo site_url(); ?>" class="btn btn-primary pull-left"><?php echo lang('delprofile_button_1'); ?></a>
			<a href="<?php echo site_url('profile/delete/now'); ?>" class="btn btn-default pull-right"><?php echo lang('delprofile_button_2'); ?></a>
			<div style="clear: both;"></div>
		</div>
	</div>
  </div>
</div>