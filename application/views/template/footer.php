      </div>
    </div>

    <!-- <div class="container-fluid bg" style="margin-top: 30px; height: 10px;">
      <div class="container">
        &nbsp;
      </div>
    </div> -->

    <div class="footer-holder">
      <div class="container">
        <footer style="font-size: 12px;">
          <div style="clear: both;"></div>

          <div class="row">
            <div class="col-sm-2">
              <h6><?php echo lang('additional_links'); ?></h6>
              <a href="<?php echo site_url('terms-and-conditions'); ?>"><?php echo lang('termsnconditions'); ?></a><br/> 
              <a href="<?php echo site_url('about'); ?>"><?php echo lang('about'); ?></a><br/> 
              <a href="<?php echo site_url('sitemap'); ?>"><?php echo lang('sitemap_link_10'); ?></a><br/> 
              <a href="<?php echo site_url('contact'); ?>"><?php echo lang('sitemap_link_11'); ?></a>
            </div>
            <div class="col-sm-2">
              <h6><?php echo lang('mailing_adress'); ?></h6>
              Universitaire Boeken<br/>
              St Adrianusstraat 28<br/>
              5614EP, Eindhoven
            </div>
            <div class="col-sm-4">
              <h6><?php echo lang('visitor_adress'); ?></h6>
              <em><?php echo lang('by_appointment'); ?></em><br/>
              <?php echo lang('v_adress'); ?>
            </div>
            <div class="col-sm-2">
              <h6>Universitaire Boeken</h6>
              <?php echo lang('part_of'); ?> <a href="http://universitaireboeken.nl">universitaireboeken.nl</a><br/>
              <strong>KVK:</strong> 55942954<br/>
              <strong>BTW:</strong> NL222099653B01
            </div>
            <div class="col-sm-2">
              <h6>TU/e Boeken</h6>
              &copy; 2014 - <?php echo date('Y'); ?><br/>Universitaire Boeken<br/>
              <?php echo lang('no_partner'); ?>
            </div>
          </div>
        </footer>

      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="<?php echo base_url('theme/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('theme/js/parallax.min.js'); ?>"></script>
    <script src="<?php echo base_url('theme/js/jquery.shorten.js'); ?>"></script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-49097176-1', 'auto');
      ga('send', 'pageview');

    </script>

    <script type="text/javascript">
      $(document).ready(function(){
        $('#par').parallax("0%", 0.3);
        $('#shortenThisLongUselessDecription').shorten();
      });
    </script>
  </body>
</html>