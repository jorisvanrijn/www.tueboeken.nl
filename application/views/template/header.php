<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $TITLE; ?></title>

    <link href="<?php echo base_url('theme/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('theme/css/tueboeken.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('theme/css/select2.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('theme/css/bootstrap-tagsinput.css'); ?>" rel="stylesheet">

    <link rel="icon" href="<?php echo base_url('theme/favicon.ico'); ?>" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo base_url('theme/favicon.ico'); ?>" type="image/x-icon"/>

    <?php echo $EXTRA_HEADER; ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <div class="white-bg">
      <div class="container" id="first-cont">
        <div class="row"> 
          <div class="col-sm-9" id="logo">
              <a href="<?php echo site_url(); ?>">
                  <img src="<?php echo base_url('theme/img/tueboeken.png'); ?>"/>
                  <h2><?php echo lang('site_title'); ?></h2>
              </a>
          </div>
          <div class="col-sm-3 hidden-xs hidden-sm" id="user">
            <div class="well well-sm">
              <div class="row">
                <div class="col-sm-4">
                  <img src="<?php echo user_icon(); ?>" class="img-responsive">
                </div>
                <?php if($logged_in){ ?>
                <div class="col-sm-8 text-right" style="padding-right: 30px;">
                  <h4><?php echo lang('header_welcome'); ?> <?php echo $user['name']; ?></h4>
                  <a href="<?php echo site_url('profile'); ?>"><?php echo lang('profile_box_profile'); ?></a> | <a href="<?php echo site_url('profile/logout'); ?>"><?php echo lang('profile_box_logout'); ?></a>
                </div>
                <?php } else { ?>
                <div class="col-sm-8 text-right" style="padding-right: 30px;">
                  <h4><?php echo lang('profile_box_title'); ?></h4>
                  <a href="<?php echo site_url('login'); ?>"><?php echo lang('profile_box_login'); ?></a> <?php echo lang('profile_box_or'); ?> <a href="<?php echo site_url('register'); ?>"><?php echo lang('profile_box_register'); ?></a>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="container-fluid bg" id="par">
        <div class="container">
          <div class="well well-sm" id="search-etc">
            <div class="row">
              <div class="col-sm-6">
                 <?php echo form_open(site_url(),array('class'=>'form-horizontal')); ?>
                 <div class="input-group">
                  <input type="text" name="searchQuery" class="form-control" placeholder="<?php echo lang('search_text'); ?>"/>
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="submit"><?php echo lang('search_button'); ?></button>
                  </span>
                 </div><!-- /input-group -->
                 </form>
              </div>
              <div class="col-sm-6 text-right">
                <ul class="nav nav-pills pull-right">
                  <li <?php if ($active == 'home') echo 'class="active"'; ?>><a href="<?php echo site_url(); ?>"><?php echo lang('main_nav_home'); ?></a></li>
                  <li <?php if ($active == 'addbook') echo 'class="active"'; ?>><a href="<?php echo site_url('addbook'); ?>"><?php echo lang('main_nav_buynsell'); ?></a></li>
                  <?php if($logged_in){ ?>
                  <li <?php if ($active == 'book') echo 'class="active"';  else 'class="hidden-sm hidden-xs hidden-md"'; ?>><a href="<?php echo site_url('boeken-A-Z'); ?>"><?php echo lang('main_nav_books'); ?></a></li>
                  <li <?php if ($active == 'profile/logout') echo 'class="active"';  else 'class="hidden-sm hidden-xs hidden-md"'; ?>><a href="<?php echo site_url('profile/logout'); ?>"><?php echo lang('main_nav_logout'); ?></a></li>
                  <?php } else { ?>
                  <li <?php if ($active == 'book') echo 'class="active"';  else 'class="hidden-sm hidden-xs hidden-md"'; ?>><a href="<?php echo site_url('boeken-A-Z'); ?>"><?php echo lang('main_nav_books'); ?></a></li>
                  <li <?php if ($active == 'register') echo 'class="active"'; ?>><a href="<?php echo site_url('register'); ?>"><?php echo lang('profile_box_register'); ?></a></li>
                  <!-- <li <?php if ($active == 'login') echo 'class="active"'; ?>><a href="<?php echo site_url('login'); ?>">Login</a></li>-->
                  <?php } ?>
                </ul>
                <div style="clear: both;"></div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="container" id="content">