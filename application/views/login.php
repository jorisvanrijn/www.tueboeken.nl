<div class="row marketing">

    <div class="col-sm-6 col-sm-offset-3">

        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-heading">
                <div class="panel-title"><h4 style="margin: 0;"><?php echo lang('login_panel_title_more'); ?></h4></div>
            </div>

            <div style="padding-top:30px" class="panel-body">
            <?php if($page == 'login') { echo validation_errors(); } ?>
            
            <?php echo form_open('home/login/verify',array('class'=>'form-horizontal')); ?>
            <?php echo loginForm(); ?>
            </form>
            </div>
        </div>
    </div>
</div>