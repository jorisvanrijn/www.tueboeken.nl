<div class="row">
	<div class="col-xs-6">
		<h2>Succes met je <strong>tentamens</strong></h2>
		<h4>Wij zijn ook aan het blokken..</h4>
		<p>Wij van het TU/e boeken team wensen je veel succes met het leren van je tentamens. Heb je er dan genoeg van je boeken? Dan willen we natuurlijk graag dat je ze op TU/e boeken zet! Verkoop gratis je boeken van dit kwartiel of koop ze van je medestudent!</p>
	</div>
	<div class="col-xs-6">
		<h2>Ons <strong>verdienmodel</strong></h2>
		<h4>We blijven altijd gratis</h4>
		<p>Deze nieuwsbrief leek ons een goede gelegenheid om beknopt uit te leggen hoe wij onze uitgaven aan de site terug verdienen. Binnenkort gaan we op sommige plekken op de website reclame plaatsen. Dit doen we zodat we onze services gratis aan studenten kunnen blijven aanbieden!</p>
	</div>
</div>

<hr/>

<div class="row">
	<div class="col-xs-6">
		<a href="http://tueboeken.nl/addbook" class="btn btn-primary btn-lg btn-block">Verkoop boeken van<br/>kwartiel 3</a>
	</div>
	<div class="col-xs-6">
		<a href="http://tueboeken.nl/boeken-A-Z" class="btn btn-default btn-lg btn-block">Koop boeken voor<br/>kwartiel 4</a>
	</div>
</div>