<br/>
<center><h3><?php echo lang('sitemap_link_title'); ?></h3></center>
<hr/>
<div class="row">
	<div class="col-sm-4 col-sm-offset-2">
		<div class="list-group">
			<a class="list-group-item" href="<?php echo site_url('home'); ?>"><?php echo lang('sitemap_link_1'); ?></a>
			<a class="list-group-item" href="<?php echo site_url('boeken-A-Z'); ?>"><?php echo lang('sitemap_link_2'); ?></a>
			<a class="list-group-item" href="<?php echo site_url('login'); ?>"><?php echo lang('sitemap_link_3'); ?></a>
			<a class="list-group-item" href="<?php echo site_url('register'); ?>"><?php echo lang('sitemap_link_4'); ?></a>
		</div>
		<div class="list-group">
			<a class="list-group-item" href="<?php echo site_url('addbook'); ?>"><?php echo lang('sitemap_link_5'); ?></a>
			<a class="list-group-item" href="<?php echo site_url('profile'); ?>"><?php echo lang('sitemap_link_6'); ?></a>
			<a class="list-group-item" href="<?php echo site_url('profile/edit'); ?>"><?php echo lang('sitemap_link_7'); ?></a>
		</div>
	</div>

	<div class="col-sm-4">
		<div class="list-group">
			<a class="list-group-item" href="<?php echo site_url('terms-and-conditions'); ?>"><?php echo lang('sitemap_link_8'); ?></a>
			<a class="list-group-item" href="<?php echo site_url('about'); ?>"><?php echo lang('sitemap_link_9'); ?></a>
			<a class="list-group-item" href="<?php echo site_url('sitemap'); ?>"><?php echo lang('sitemap_link_10'); ?></a>
			<a class="list-group-item" href="<?php echo site_url('contact'); ?>"><?php echo lang('sitemap_link_11'); ?></a>
		</div>
	</div>
</div>