<div class="row">
<div class="col-sm-6">
	<h4><?php echo lang('about_title_1'); ?></h4>
	<p>
		<?php echo lang('about_text_1'); ?>
	</p>
	<hr/>
	<h4><?php echo lang('about_title_2'); ?></h4>
	<p>
		<?php echo lang('about_text_2'); ?>
	</p>
	<hr/>
	<h4><?php echo lang('about_title_3'); ?></h4>
	<p>
		<?php echo lang('about_text_3'); ?>
	</p>
</div>
<div class="col-sm-6">
    <div class="fb-like-box" data-href="https://www.facebook.com/tueboeken" style="width:100%;" data-width="585" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
</div>
</div>