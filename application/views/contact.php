<div class="row">
<div class="col-sm-6">
	<h4>Universitaire Boeken (Nederland)</h4>
	<p>
		<span class="glyphicon glyphicon-phone"></span> (06) 37 19 2688<br/>
		<span class="glyphicon glyphicon-envelope"></span> <a href="mailto:contact@universitaireboeken.nl">contact@universitaireboeken.nl</a><br/>
	</p>
	<hr/>
	<h4><?php echo lang('title_site_name_plain'); ?> (regional manager)</h4>
	<p>
		<span class="glyphicon glyphicon-phone"></span> (06) 37 19 2688<br/>
		<span class="glyphicon glyphicon-envelope"></span> <a href="mailto:joris@tueboeken.nl">joris@tueboeken.nl</a><br/>
	</p>
	<hr>
	<h4><?php echo lang('title_site_name_plain'); ?> (Sponsors & Partners)</h4>
	<p>
		<span class="glyphicon glyphicon-phone"></span> (06) 37 19 2688<br/>
		<span class="glyphicon glyphicon-envelope"></span> <a href="mailto:joris@tueboeken.nl">joris@tueboeken.nl</a><br/>
	</p>
	<hr/>
	<h4><span class="glyphicon glyphicon-envelope"></span> <?php echo lang('mailing_adress'); ?></h4>
	Universitaire Boeken<br/>
	St Adrianusstraat 28<br/>
    5614EP, Eindhoven
</div>

<div class="col-sm-6">
	<h4><?php echo lang('visitor_adress'); ?></h4>
	<p>
		<em><?php echo lang('by_appointment'); ?></em><br/>
        <?php echo lang('v_adress'); ?>
	</p>
	<hr/>
	<p>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9946.319831371811!2d5.49168395!3d51.4475075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c6d8e0e34889d7%3A0x5f3803b678fe5cdb!2sTU-terrein!5e0!3m2!1snl!2s!4v1394552369322" height="300" frameborder="0" style="border:0; width:100%;"></iframe>
	</p>
</div>
</div>