<div class="row">

    <div class="col-sm-6 col-sm-offset-3">

        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-heading">
                <div class="panel-title"><h4 style="margin: 0;">You've got <strong>mail</strong></h4></div>
            </div>

            <div style="padding-top:30px" class="panel-body">
            
            <p>If your email address was found in our database, then there is a new password waiting for you in your mailbox.</p>
            
            </div>
        </div>
    </div>
</div>