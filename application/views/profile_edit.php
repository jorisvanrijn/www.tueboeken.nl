<h3><?php echo lang('profile_change_title'); ?></h3>
<?php echo $msg; ?>
<hr>
<div class="row">
  <div class="col-md-7">
    <h4><?php echo lang('info_change_title'); ?></h4><br/>

    <form action="" method="POST">
      <div class="row">
        <div class="col-md-3">
          <p><?php echo lang('home_login_name'); ?></p>
        </div> 
        <div class="col-md-9">
          <div class="form-group">
            <input type="text" name="name" class="form-control" id="exampleInputEmail1" placeholder="<?php echo lang('home_login_nameplaceholder'); ?>" value="<?php echo $user['name']; ?>">
          </div>
        </div> 
      </div>
      <div class="row">
        <div class="col-md-3">
          <p><?php echo lang('home_login_study'); ?></p>
        </div> 
        <div class="col-md-9">
          <div class="form-group">
            <select class="form-control" name="courses_id">
              <?php echo getCourseDropDown($user['courses_id']); ?>
            </select>
          </div>
        </div> 
      </div>
      <div class="row">
        <div class="col-md-3">
          <p><?php echo lang('home_login_email'); ?></p>
        </div> 
        <div class="col-md-9">
          <div class="form-group">
            <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="<?php echo lang('home_login_emailplaceholder'); ?>" value="<?php echo $user['email']; ?>">
          </div>
        </div> 
      </div>
      <div class="row">
        <div class="col-md-3">
          <p><?php echo lang('info_change_startyear'); ?></p>
        </div> 
        <div class="col-md-9">
          <div class="form-group">
            <input type="number" min="1970" max="<?php echo (date('Y') + 1); ?>" name="startyear" class="form-control" placeholder="" value="<?php echo $user['startyear']; ?>">
          </div>
        </div> 
      </div>
      <div class="row">
        <div class="col-md-3">
          <p><?php echo lang('info_change_phone'); ?></p>
        </div> 
        <div class="col-md-9">
          <div class="form-group">
            <input type="tel" name="tel" class="form-control" id="exampleInputTel1" placeholder="<?php echo lang('info_change_phone_placeholder'); ?>" value="<?php echo $user['phone']; ?>">
          </div>
        </div> 
      </div>
      <div class="row">
        <div class="col-md-3">
          <p><?php echo lang('info_change_description'); ?></p>
        </div> 
        <div class="col-md-9">
          <div class="form-group">
            <textarea name="description" class="form-control" style="width:100%;height:100px;" placeholder="<?php echo lang('info_change_description_placeholder'); ?>"><?php echo $user['description']; ?></textarea>
          </div>
          <button type="submit" class="btn btn-primary"><?php echo lang('info_change_submit'); ?></button>
        </div> 
      </div>
    </div>
  </form>

  <div class="col-md-5">
    <div class="well well-sm" style="padding: 30px;">
      <h4 style="margin-top: 0;"><?php echo lang('password_change_title'); ?></h4><br/>

      <form action="" method="POST">
        <div class="row">
          <div class="col-md-3">
            <p><?php echo lang('editprofile_old'); ?></p>
          </div> 
          <div class="col-md-9">
            <div class="form-group">
              <input name="old_password" type="password" class="form-control" id="exampleInputEmail1" placeholder="Voer je oude wachtwoord in">
            </div>
          </div> 
        </div>
        <div class="row">
          <div class="col-md-3">
            <p><?php echo lang('editprofile_new'); ?></p>
          </div> 
          <div class="col-md-9">
            <div class="form-group">
              <input name="password_1" type="password" class="form-control" id="exampleInputEmail1" placeholder="Voer een nieuw wachtwoord in">
            </div>
          </div> 
        </div>
        <div class="row">
          <div class="col-md-3">
            <p><?php echo lang('editprofile_repeat'); ?></p>
          </div> 
          <div class="col-md-9">
            <div class="form-group">
              <input name="password_2" type="password" class="form-control" id="exampleInputEmail1" placeholder="Herhaal je nieuwe wachtwoord">
            </div>
            <button type="submit" class="btn btn-primary"><?php echo lang('editprofile_savepass'); ?></button>
          </div> 
        </div>
      </form>
    </div>
  </div>
</div>
<br/>
<hr/>
<div class="text-right">
  <?php echo lang('editprofile_otheractions'); ?>: <a href="<?php echo site_url('profile/delete'); ?>" class="btn btn-danger btn-sm"><?php echo lang('editprofile_deleteprofile'); ?></a>
</div>