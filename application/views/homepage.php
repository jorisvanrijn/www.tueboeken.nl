<div class="row">
  <div class="col-sm-6">
    <div class="quote text-center">
      <h2><?php echo lang('home_lang_subtitle'); ?></h2>
      <h3><?php echo lang('home_lang_subtitle_2'); ?></h3>

      <h2><strong><?php echo $numberOfAdvertisements; ?></strong> ads for <strong><?php echo $numberOfBooks; ?></strong> books, <br/>helping <strong><?php echo $numberOfStudents; ?></strong> students</h2>
      <h3>and bit by bit we're growing!</h3>
    </div>
  </div>
  <div class="col-sm-6 visible-lg">
    <div class="fb-like-box" data-href="https://www.facebook.com/tueboeken" style="width:100%;" data-width="585" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
  </div>
  <div class="col-sm-6 visible-md">
    <div class="fb-like-box" data-href="https://www.facebook.com/tueboeken" style="width:100%;" data-width="445" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
  </div>
  <div class="col-sm-6 visible-sm">
    <div class="fb-like-box" data-href="https://www.facebook.com/tueboeken" style="width:100%;" data-width="345" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
  </div>
</div>

<hr/>

<div class="row">
  <?php echo bookHTML( array_slice($books, 0, 2) ); ?>
</div>

<?php if(!$logged){ ?> 

  <hr/>
  <div class="row">
  	<div class="col-sm-8"><div class="well well-default">
  		<h2><span class="glyphicon glyphicon-book text-muted"></span> <?php echo lang('register_panel_title'); ?></h2>
      <hr/>

      <?php echo form_open('home/register/verify',array('class'=>'form-horizontal')); ?>
      <?php echo registerForm(); ?>
      </form>


  	</div></div>
  	<div class="col-sm-4"><div class="well well-default">
  		<h2><span class="glyphicon glyphicon-user text-muted"></span> <?php echo lang('login_panel_title'); ?></h2>
      <hr/>

      <?php echo form_open('home/login/verify',array('class'=>'form-horizontal')); ?>
      <?php echo loginForm(); ?>
      </form>
  	</div></div>
  </div>

<?php }else{ ?>

  <hr/>
  <div class="row hidden-xs">
    <?php echo bookHTML( array_slice($books, 6, 2) ); ?>
  </div>

<?php } ?>
<hr/>

<div class="row hidden-xs">
  <?php echo bookHTML( array_slice($books, 2, 2) ); ?>
</div>
<hr/>
<div class="row hidden-xs">
  <?php echo bookHTML( array_slice($books, 4, 2) ); ?>
</div>
<hr/>

<div class="row">
  <div class="col-sm-12">
    <a href="<?php echo site_url('boeken-A-Z'); ?>" class="btn btn-block btn-default"><?php echo lang('all_books'); ?></a>
  </div>
</div>