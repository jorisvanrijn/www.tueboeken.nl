<div class="container">
    <div class="row">
        <div class="col-sm-3 col-md-2">
            <h3><?php echo lang('search_filter_title'); ?></h3>
            <?php if($result_rows > 0) { ?>
            <strong><?php echo lang('search_filter_by'); ?></strong>
            <div class="list-group">
            <?php foreach($courseList as $course) { ?>
              <a href="<?php echo base_url('search/'.$course); ?>" class="list-group-item"><?php echo ($course == $query)? '<strong>'.$course.'</strong>' : $course; ?></a>
            <?php } ?>
            </div>
            <?php } ?>
        </div>
        <div class="col-sm-9 col-md-10">
            <h2 style="margin-left: 10px;"><?php echo lang('search_searches_found'); ?> '<?php echo $query; ?>'</h2>

<?php if($result_rows > 0) { ?>

            <em class="text-muted" style="font-size: 12px; margin-left: 10px;"><?php echo $result_rows; ?> <?php echo lang('search_advertisements'); ?> <?php /* on <?php echo $pages; ?> page(s) */ ?></em>
            <br>
            <hr>


<?php foreach($results as $book) { ?>
            <div class="row">

                <div class="col-sm-2">
                    <a href="<?php echo $book['url']; ?>"><img class="book-thumb img-responsive" src="<?php echo bookImage($book['image']); ?>">
                    </a>
                </div>

                <div class="col-sm-10">
                    <h4 class="title"><?php echo $book['title']; ?></h4>
                    <h6 class="subtitle"><?php echo $book['subtitle']; ?></h6>
                    <div class="desc"><em><?php echo $book['author']; ?></em>
                        <p><?php echo $book['shortdescription']; ?></p>
                    </div>
                    <p><a href="<?php echo $book['url']; ?>" class="btn btn-primary btn-sm"><?php echo lang('offers_past_tence'); ?> <span class="badge"><?php echo $book['offered']; ?></a>
                    <a href="<?php echo $book['url']; ?>" class="btn btn-default btn-sm"><?php echo lang('requests_past_tence'); ?> <span class="badge"><?php echo $book['requested']; ?></a></p>
                </div>

            </div>

            <hr>
<?php } ?>

<?php echo $page_numbers; ?>

<?php /*
            <ul class="pagination">
                <li class="disabled"><a>«</a>
                </li>
                <li class="active"><a href="http://tueboeken.nl/search/1/?q=calculus">1</a>
                </li>
                <li><a href="http://tueboeken.nl/search/2/?q=calculus">2</a>
                </li>
                <li><a href="http://tueboeken.nl/search/3/?q=calculus">3</a>
                </li>
                <li><a href="http://tueboeken.nl/search/2/?q=calculus">»</a>
                </li>
            </ul>
*/ ?>


<?php } else { ?>

		<hr>
			<p><?php echo lang('not_found_question_1'); ?></p>
		<hr>

<?php } ?>
        </div>
    </div>
</div>