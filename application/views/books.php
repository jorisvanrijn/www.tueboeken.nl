<div class="row">
	<div class="col-sm-12">
		<h2>Books A-Z</h2>

		<center>
			<hr/>
			<nav>
				<ul class="pagination">
				  <?php
				  	if($letter == ''){
				  		echo '<li class="active"><a href="'.site_url('boeken-A-Z').'">All</a></li>';
				  	}else{
				  		echo '<li><a href="'.site_url('boeken-A-Z').'">All</a></li>';
				  	}

				  	foreach(range('A','Z') as $lt){
				  		if($lt == $letter){
				  			echo '<li class="active"><a href="'.site_url('boeken-A-Z/'.$lt).'">'.$lt.'</a></li>';
				  		}else{
				  			echo '<li><a href="'.site_url('boeken-A-Z/'.$lt).'">'.$lt.'</a></li>';
				  		}
				  	}
				  ?>
				</ul>
			</nav>
			<hr/>
		</center>
	</div>

	<div class="col-sm-12">
		<?php
			if(is_array($books) AND count($books) > 0){
				foreach($books as $book){
					//var_dump($book);
					?>
					<a href="<?php echo $book['url']; ?>" class="list-group-item">
						<div class="row">
							
							<div class="col-sm-9">
								<strong><?php echo $book['title']; ?></strong><br/>
								<?php echo ($book['subtitle'] != '') ? '<small>'.$book['subtitle'].'</small><br/>' : ''; ?>
								<em class="text-muted" style="font-size: 12px;">
									Author: <?php echo $book['author']; foreach($book['isbnlist'] as $isbn) { ?>
									<?php echo $isbn['name']; ?>: <?php echo $isbn['value']; ?><?php } ?>
								</em>
							</div>

							<div class="col-sm-3 text-right">
								<span class="btn btn-sm btn-primary"><?php echo lang('offers_past_tence'); ?>: <?php echo $book['offered']; ?></span> 
								<span class="btn btn-sm btn-default"><?php echo lang('requests_past_tence'); ?>: <?php echo $book['requested']; ?></span>
							</div>

						</div>
					</a>
					<?php
				}
			}else{
				echo '<div class="alert alert-warning"><center><strong>'.lang('sorry').'</strong><br/>'.lang('starting_with_not_found').' \''.$letter.'\'</center></div>';
			}
		?>
	</div>
</div>