<div class="row" style="margin-top: 20px;">
	<div class="col-sm-4">
		<div class="well well-default">
			<?php
			if($ownProfile){
				?>
					
					<a class="btn btn-primary btn-sm btn-block" href="<?php echo site_url('profile/edit'); ?>"><i class="glyphicon glyphicon-pencil"></i> <?php echo lang('edit_profile'); ?></a>
					<hr/>
				<?php
				}
			?>
			<div class="row">
				<div class="col-sm-12">
					<h3 style="margin-top: 10px; margin-bottom: 0;"class="no-underline"><?php echo userLink($currentUser['id']);?></h3>
					<em><?php echo userCourse($currentUser['id']); ?></em>
				</div>
			</div>
			<hr/>
				<?php echo ($currentUser['description'] != '') ? $currentUser['description'] : '<em> '.lang('no_desc').' </em>'; ?>
				<?php echo ($ownProfile && ($currentUser['description'] == '')) ? '<a href="'.site_url('profile/edit').'">'.lang('change_this').' &raquo;</a>' : ''; ?>
			<hr/>
				<table class="table table-bordered table-striped">
					<tr>
						<td style="width: 10%;"><i class="glyphicon glyphicon-phone-alt"></i></td>
						<td><?php echo ($currentUser['phone'] != '') ? $currentUser['phone'] : '-'; ?></td>
					</tr>
					<tr>
						<td style="width: 10%;"><i class="glyphicon glyphicon-envelope"></i></td>
						<td><a href="mailto:<?php echo $currentUser['email']; ?>"><?php echo $currentUser['email']; ?></a></td>
					</tr>
				</table>
			<hr/>

			<table class="table table-bordered table-striped">
				<tr>
					<td style="width: 60%;"><strong><?php echo lang('found_sold'); ?>:</strong></td>
					<td><?php echo $stat['sold']; ?> <?php echo lang('profile_books'); ?></td>
				</tr>
				<tr>
					<td><strong>Currently <span class="label label-primary"><?php echo lang('offers_past_tence'); ?></span></strong></td>
					<td><?php echo $stat['offered']; ?> <?php echo lang('profile_books'); ?></td>
				</tr>
				<tr>
					<td><strong>Currently <span class="label label-default"><?php echo lang('requests_past_tence'); ?></span></strong></td>
					<td><?php echo $stat['requested']; ?> <?php echo lang('profile_books'); ?></td>
				</tr>
			</table>
		</div>
	</div>

	<div class="col-sm-8">
		<h2 style="margin-top: 0;"><?php echo ($ownProfile) ? lang('my_ads') : $currentUser['name'] . '\'s '.lang('other_ads'); ?> <span class="badge"><?php echo $stat['offered']+$stat['requested']; ?></span></h2>

		<?php
			if(isset($offerList) AND is_array($offerList) AND count($offerList) > 0){
				?>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th><?php echo lang('profile_book'); ?></th>
							<th><?php echo lang('profile_type'); ?></th>
							<th><?php echo lang('profile_since'); ?></th>
							<?php if($ownProfile){ ?><th><?php echo lang('profile_actions'); ?></th><?php } ?>
						</tr>
					</thead>
				<?php
				foreach($offerList as $ad){
					//var_dump($ad);
					$book = $ad['book'];
					?>
					<tr><td class="no-underline black-link">
						<a href="<?php echo site_url('book/'.$book['slug']); ?>" style="text-decoration: none;"><strong><?php echo $book['title']; ?></strong><br/>
						<small class="text-muted" style="font-size: 12px;">
							<?php echo $ad['description']; ?>
						</small></a>
					</td><td>
						<?php echo ($ad['isforsale'] == 1) ? '<span class="label label-primary">'.lang('offers_past_tence').'</span>' : '<span class="label label-default">'.lang('requests_past_tence').'</span>'; ?>
					</td><td>
						<?php echo date('F d\<\s\u\p\>S\<\/\s\u\p\>, Y', strtotime($ad['created'])); ?>
					</td><?php if($ownProfile){ ?><td>
						<p><a href="<?php echo $ad['url']; ?>/edit" class="btn btn-xs btn-primary"><?php echo lang('profile_edit'); ?></a></p>
						<p><a href="<?php echo site_url('book/delete/'.$ad['id']); ?>" class="btn btn-xs btn-default"><?php echo lang('profile_delete'); ?></a></p>
					</td><?php } ?></tr>
					<?php
				}
				?>
				</table>
				<?php
			}else{
				if($ownProfile){
					echo '<div class="alert alert-warning"><strong>'.lang('whoa').'</strong><br/>'.lang('no_books_profile_1').' <a href="'.site_url('addbook').'" class="btn btn-xs btn-danger">'.lang('no_books_profile_2').'</a> '.lang('no_books_profile_3').'</div>';
				}else{
					echo '<div class="alert alert-info"><strong>'.lang('sorry').', '.$user['name'].'!</strong><br/>'.$currentUser['name'].' '.lang('no_books_profile_4').'</div>';
				}
			}
		?>

		<?php
			if($ownProfile){ ?>
			<hr/>
			<h2><?php echo lang('profile_latest_replies'); ?> <span class="badge"><?php echo (isset($replies) AND is_array($replies)) ? count($replies) : 0; ?></span></h2>
			<?php
				if(isset($replies) AND is_array($replies) AND count($replies) > 0){
					?>
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><?php echo lang('profile_message'); ?></th>
								<th><?php echo lang('profile_info'); ?></th>
								<th><?php echo lang('profile_actions'); ?></th>
							</tr>
						</thead>
					<?php
					foreach($replies as $reply){
						//var_dump($reply);
						?>
						<tr><td>
							<?php echo nl2br($reply['message']); ?>
						</td><td>
							On: <strong><?php echo date('F d\<\s\u\p\>S\<\/\s\u\p\>, Y', strtotime($ad['created'])); ?></strong><hr/>
							<?php echo userLink($reply['user_id']); ?><br/>
							<?php echo userCourse($reply['user_id']); ?><br/>
							<em><?php echo userMail($reply['user_id']); ?></em>

						</td><td>
							<p><a href="mailto:<?php echo userMail($reply['user_id']); ?>" class="btn btn-xs btn-primary"><?php echo lang('profile_mail'); ?></a></p>
						</td></tr>
						<?php
					}
					?>
					</table>
					<?php
				}else{
					echo '<div class="alert alert-warning"><strong>'.lang('hmm').'</strong><br/>'.lang('no_replies_profile').'</div>';
				}
			?>
			<?php }
		?>

	</div>
</div>