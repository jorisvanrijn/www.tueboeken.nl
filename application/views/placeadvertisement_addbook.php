<?php echo form_open('addbook/manually/',array('class'=>'form-horizontal')); ?>
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="panel panel-default step" id="step1">
                <div class="panel-heading"><h4 style="margin: 0;"><?php echo lang('add_step_1'); ?></h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-4"><strong><?php echo lang('book_isbn'); ?></strong>
                            <br><em style="font-size: 11px;" class="text-muted"><?php echo lang('book_isbn_note'); ?></em>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" value="<?php echo set_value('isbn'); ?>" class="form-control" name="isbn" placeholder="<?php echo lang('book_isbn_placeholder'); ?>">
                            </div>
                            <?php echo form_error('isbn','<div class="alert alert-danger" style="margin-top: 10px; padding-top: 5px; padding-bottom: 5px;">','</div>'); ?>
                            </span>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-4"><strong><?php echo lang('book_title'); ?></strong>
                            <br><em style="font-size: 11px;" class="text-muted"><?php echo lang('book_title_note'); ?></em>
                        </div>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <input type="text" value="<?php echo set_value('title'); ?>" class="form-control " name="title" placeholder="<?php echo lang('book_title_placeholder'); ?>">
                            </div>

                            <?php echo form_error('title','<div class="alert alert-danger" style="margin-top: 10px; padding-top: 5px; padding-bottom: 5px;">','</div>'); ?>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 text-right">
            <div class="belowscreen">
                <a href="javascript:history.go(-1);" class="btn btn-primary vorige"><?php echo lang('prev'); ?></a>
                <input type="submit" class="btn btn-primary volgende" value="<?php echo lang('next'); ?>">
            </div>
        </div>
    </div>
</form>