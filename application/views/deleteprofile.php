<div class="container">
  <div class="row">
  	<div class="col-sm-4 col-sm-offset-4">
		<div class="well well-default text-center">
			<h3 style="margin-top: 0;">Are you sure you want to delete your profile?<br/><strong>There is no Ctrl+Z after this!</strong></h3>
			
			<a href="<?php echo site_url(); ?>" class="btn btn-primary pull-left">No</a> <a href="<?php echo site_url('profile/delete/now'); ?>" class="btn btn-default pull-right">Yes</a>
			<div style="clear: both;"></div>
		</div>
	</div>
  </div>
</div>