<div class="row">

    <div class="col-sm-6 col-sm-offset-3">

        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-heading">
                <div class="panel-title"><h4 style="margin: 0;"><?php echo lang('forgot_pass_panel_title'); ?></h4></div>
            </div>

            <div style="padding-top:30px" class="panel-body">
            
            <p><?php echo lang('forgot_pass_panel_text'); ?></p>

            <?php echo validation_errors('<div class="alert alert-danger">','</div>'); ?>
            
            <?php echo form_open('home/forgot',array('class'=>'form-horizontal')); ?>
                <div style="margin-bottom: 25px" class="input-group">
                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input id="login-username" type="text" class="form-control" name="forgot_email" value="" placeholder="Email">
                </div>

                <div style="margin-top:10px" class="form-group">
                    <!-- Button -->

                    <div class="col-sm-12 controls">
                        <input type="submit" id="btn-login" value="Request new password" class="btn btn-primary">&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url('login'); ?>" class="text-muted" style="font-size: 11px;"><?php echo lang('forgot_pass_panel_link'); ?></a>

                    </div>
                </div>

            </form>
            </div>
        </div>
    </div>
</div>