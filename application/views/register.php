<div class="row marketing">

    <div class="col-sm-8">
        <h3><?php echo lang('home_login_noaccount'); ?></h3>
        <em><?php echo lang('home_login_noaccountsub'); ?></em>
        <hr>
        <?php echo form_open('home/register/verify',array('class'=>'form-horizontal')); ?>
        <?php echo registerForm($showRegisterErrors); ?>
        </form>
    </div>


    <div class="col-sm-4">

        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-heading">
                <div class="panel-title"><?php echo lang('home_login_login'); ?></div>
            </div>

            <div style="padding-top:30px" class="panel-body">
            <?php if($page == 'login') { echo validation_errors(); } ?>
            
            <?php echo form_open('home/login/verify',array('class'=>'form-horizontal')); ?>
            <?php echo loginForm(); ?>
            </form>
            </div>
        </div>
    </div>
</div>