<?php if($editMode) echo form_open($editUrl,array('class'=>'form-horizontal')); ?>
<div class="row marketing">
    <div class="col-md-8 col-sm-7" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
        <meta itemprop="availability" content="InStock">
        <meta itemprop="priceCurrency" content="EUR">
        <h2 style="display:inline-block;">
        <span itemprop="name"><?php echo $book['title']; ?></span>
        <?php if(($header['logged_in']) and ($offer['user_id'] == $header['user']['id'])) { ?>
            <?php if(!$editMode) { ?>
                <a href="<?php echo $editUrl; ?>" class="label label-primary">Edit book</a>
            <?php } ?>
        <?php } ?>
        </h2>
        <h4 style="display:inline-block;" class="text-muted"><?php echo $book['subtitle']; ?></h4>
        <hr>

        <div class="row">
            <div class="col-lg-3">
                <strong>Offered by</strong>
            </div>
            <div class="col-lg-9">
                <?php if($editMode) { ?>
                    <?php echo $offer['user']; ?>
                <?php } else { ?>
                    <?php echo $offer['user_url']; ?>
                <?php } ?>
            </div>
            <div style="clear:both;"></div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-3">
                <strong><?php echo lang('book_offer_price'); ?></strong>
            </div>
            <div class="col-lg-9">
                <?php if($editMode) { ?>
                    <div class="input-group" style="width:200px;">
                        <span class="input-group-addon">&euro;</span>
                        <input type="number" min="0.01" step="0.01" max="300.00" value="<?php echo $offer['price']; ?>" autofocus="" class="form-control ad_price" name="price" placeholder="00.00">
                    </div>
                    <?php echo form_error('price','<div class="alert alert-danger" style="margin-top: 10px; padding-top: 5px; padding-bottom: 5px;">','</div>'); ?>
                <?php } else { ?>
                    <span class="h4">&euro; <span itemprop="price"><?php echo $offer['price']; ?></span></span>
                <?php } ?>
                
            </div>
            <div style="clear:both;"></div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-3">
                <strong><?php echo lang('book_offer_state'); ?></strong>
            </div>
            <div class="col-lg-9" itemprop="itemCondition">
                <?php if($editMode) { ?>
                    <div class="input-group">
                        <select name="state" style="min-width:200px;" class="advselect form-control" id="a28906831" tabindex="-1">
                            <?php

                                foreach($status as $k => $v){
                                    echo '<option value="'.$k.'" '.(($offer['state_id'] == $k)? 'selected' : '').'>'.$v.'</option>';
                                }

                            ?>
                        </select>

                    </div>
                <?php } else { ?>
                    <?php echo $offer['state']; ?>
                <?php } ?>
            </div>
            <div style="clear:both;"></div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-3">
                <strong><?php echo lang('book_offer_description'); ?></strong>
            </div>
            <div class="col-lg-9">
                <?php if($editMode) { ?>
                    <textarea name="description" class="form-control"><?php echo $offer['description']; ?></textarea>
                    <?php echo form_error('description','<div class="alert alert-danger" style="margin-top: 10px; padding-top: 5px; padding-bottom: 5px;">','</div>'); ?>
                <?php } else { ?>
                    <?php echo $offer['description']; ?>
                <?php } ?>
            </div>
            <div style="clear:both;"></div>
        </div>
        <hr>
        <span class="st_facebook_large" displaytext="Facebook" st_processed="yes"><span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton"><span class="stLarge" style="background-image: url(http://w.sharethis.com/images/facebook_32.png);"></span></span>
        </span>
        <span class="st_twitter_large" displaytext="Tweet" st_processed="yes"><span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton"><span class="stLarge" style="background-image: url(http://w.sharethis.com/images/twitter_32.png);"></span></span>
        </span>
        <span class="st_googleplus_large" displaytext="Google +" st_processed="yes"><span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton"><span class="stLarge" style="background-image: url(http://w.sharethis.com/images/googleplus_32.png);"></span></span>
        </span>
        <span class="st_linkedin_large" displaytext="LinkedIn" st_processed="yes"><span style="text-decoration:none;color:#000000;display:inline-block;cursor:pointer;" class="stButton"><span class="stLarge" style="background-image: url(http://w.sharethis.com/images/linkedin_32.png);"></span></span>
        </span>
        <hr>

<?php if(($header['logged_in']) and ($offer['user_id'] == $header['user']['id'])) { ?>
        <div class="row">
            <div class="col-sm-12">
                <?php if(!$editMode) { ?>
                    <hr>
                    <h2>Options</h2>
                    <hr>
                    <a href="<?php echo $editUrl; ?>" class="btn btn-primary">Edit book</a>
                    <a href="<?php echo site_url('book/delete/'.$offer['id']); ?>" class="btn btn-default">Delete book</a>
                <?php } else { ?>
                    <input type="submit" class="btn btn-primary" value="Save changes">
                <?php } ?>
            </div>
        </div>

<?php } elseif($header['logged_in']) { ?>

        <?php echo validation_errors('<div class="alert alert-danger">','</div>'); ?>
            
        <?php echo form_open($offerUrl,array('class'=>'form-horizontal')); ?>
            <div class="panel panel-default" style="margin-top: 15px;">
                <div class="panel-heading">
                    <h4><?php echo lang('book_respond'); ?></h4>
                </div>
                <div class="panel-body" id="boekInfo">

                    <div class="row">
                        <div class="col-sm-12">
                            <p>
                                <strong><?php echo lang('book_respond_message'); ?></strong>
                                <br><em style="font-size: 11px;" class="text-muted"><?php echo lang('book_respond_message_subtitle'); ?></em>
                            </p>
                            <textarea id="reply_message" name="reply_message" class="form-control" rows="9"><?php echo str_replace(array('[seller]','[title]','[name]'),array($offer['user'],$book['title'],$header['user']['name']),lang('book_respond_message_value')); ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <input type="submit" value="Send" class="btn btn-primary">
                </div>
            </div>
        </form>
<?php } else { ?>

        <p>You are not logged in. <a href="<?php echo site_url('login'); ?>">Login</a> or <a href="<?php echo site_url('register'); ?>">register</a> to reply.</p>

<?php } ?>
    </div>


    <div class="col-md-4 col-sm-5" itemscope itemtype="http://schema.org/Book">
        <meta itemprop="bookEdition" content="text">

        <div class="panel panel-default">
            <div class="panel-heading">
                <h5>General book information</h5>
            </div>
            <div class="panel-body" id="boekInfo">
                <img itemprop="image" src="<?php echo bookImage($book['image']); ?>" alt="<?php echo $book['title']; ?>" class="image img-responsive img-thumbnail pull-left" style="margin-right: 5px;">

                <h4 class="title" itemprop="name" style="margin:0;"><?php echo $book['title']; ?></h4>
                <h6 class="subtitle" style="margin:0;"> <?php echo $book['subtitle']; ?></h6>

                <div style="clear: both; height:20px;">&nbsp;</div>

                <div class="tablelist">
                    <table class="table">
                        <tbody>
                            <?php foreach($book['isbnList'] as $isbn) { ?>
                            <tr>
                                <td style="width:30%"><?php echo $isbn['name']; ?></td>
                                <td itemprop="isbn"><?php echo $isbn['value']; ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="tablelist">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td style="width:30%">Author:</td>
                                <td><?php echo $book['author']; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr>
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                  <div class="btn-group" role="group">
                    <a itemprop="url" href="<?php echo $bookUrl; ?>#offers" class="btn btn-primary <?php if($editMode) echo 'disabled'; ?>">More offers <span class="badge"><?php echo $book['advertisements_count']; ?></span></a>
                  </div>
                  <div class="btn-group" role="group">
                    <a href="<?php echo site_url('addbook/add/'.$book['id']); ?>" class="btn btn-default <?php if($editMode) echo 'disabled'; ?>">Place ad</a>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if($editMode) echo '</form>'; ?>


<?php if(!$editMode) { ?>
<div class="row">
    <div class="col-sm-12">
        <hr>
        <h2>Other advertisements of <?php echo $offer['user_url']; ?></h2>
        <hr>

        <?php if($moreOffers){ ?>
        <?php foreach($offerList as $of){ ?>
        <div class="row">

            <div class="col-xs-5 col-sm-3 col-md-2">
                <a href="<?php echo $of['url']; ?>"><img class="book-thumb img-responsive" src="<?php echo bookImage($of['book']['image']); ?>">
                </a>
            </div>

            <div class="col-xs-7 col-sm-7 col-md-8">
                <a href="<?php echo $of['url']; ?>"><h4 class="title"><?php echo $of['book']['title']; ?></h4></a>
                <h6 class="subtitle"><?php echo $of['book']['subtitle']; ?></h6>
                <div class="desc"><em><?php echo $of['book']['author']; ?></em>
                    <p><?php echo $of['book']['shortdescription']; ?></p>
                </div>
                <a href="<?php echo $of['url']; ?>" class="link btn btn-primary btn-xs">View »</a>
            </div>

            <div class="hidden-xs col-sm-2">
                <a href="<?php echo $of['url']; ?>" class="h3 text-primary">&euro; <?php echo $of['price']; ?></a>
            </div>

        </div>
        <hr>
        <?php } ?>
        <?php } else { ?>
            <p>There are no other advertisements</p>
        <?php } ?>
    </div>
</div>

<?php } ?>