<div class="container">
  <div class="row">
  	<div class="col-sm-6 col-sm-offset-3">
		<div class="well well-default text-center">
			<h3 style="margin-top: 0;"><?php echo lang('ad_del_text_1'); ?><br/><strong>(<?php echo lang('ad_del_text_2'); ?>)</strong></h3>
			
			<hr/>
			<div class="pull-left">
				<a href="<?php echo site_url('book/delete/'.$offer['id'].'/sold'); ?>" class="btn btn-primary"><?php echo lang('ad_del_button_1'); ?></a> 
				<a href="<?php echo site_url('book/delete/'.$offer['id'].'/now'); ?>" class="btn btn-default"><?php echo lang('ad_del_button_2'); ?></a>
				&nbsp;&nbsp;<a href="javascript:history.go(-1);"><?php echo lang('ad_del_button_3'); ?></a>
			</div>
			<div style="clear: both;"></div>
		</div>
	</div>
  </div>
</div>