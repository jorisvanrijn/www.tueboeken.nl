<span itemscope itemtype="http://schema.org/Book">
<meta itemprop="bookEdition" content="text">

<div class="row">
    <div class="col-sm-12">
      <h1><a href="<?php echo site_url('book/'.$book['slug']); ?>" itemprop="name"><?php echo $book['title']; ?></a><br><small class="text-muted"><?php echo $book['subtitle']; ?></small></h1>
    </div>
</div>

<div class="row">
    <hr/>
    <div class="col-sm-6 col-md-7 col-lg-8">
        <div class="row">
            <div class="col-md-4">
                <img itemprop="image" src="<?php echo bookImage($book['image'], array(230, 300)); ?>" class="img-responsive" alt="<?php echo $book['title']; ?>">
            </div>
            <div class="col-md-8">
                <table class="table table-bordered table-striped">
                  <tr>
                    <td><strong><?php echo lang('author'); ?>:</strong></td><td><?php echo $book['author']; ?></td>
                  </tr>
                <?php foreach($book['isbnList'] as $isbn) { ?>
                  <tr>
                    <td><strong><?php echo $isbn['name']; ?>:</strong></td><td itemprop="isbn"> <?php echo $isbn['value']; ?></td>
                  </tr>
                <?php } ?>

                  <tr>
                    <td><strong><?php echo lang('course_codes'); ?>:</strong></td>
                    <td>
                    <?php foreach($book['courseList'] as $course) { ?>
                    <a href="<?php echo base_url('search/'.$course['code']); ?>" class="label label-primary"><?php echo $course['code']; ?></a>
                    <?php } ?></td>
                  </tr>
                
                </table>
                <hr/>

                <p itemprop="description" id="shortenThisLongUselessDecription"><?php echo strip_tags(html_entity_decode($book['description'])); ?></p>        
            </div>
            <meta itemprop="inLanguage" content="Engels" />
        </div>
    </div>
    <div class="col-sm-6  col-md-5 col-lg-4">
      <?php if(sizeof($book['offers']) != 0){ ?>
        
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-xs-8 no-underline black-link">
                <a href="<?php echo $book['offers'][0]['url']; ?>"><strong class="text-primary">Best offer</strong></a><br>
                <span class="small text-muted"><?php echo lang('used_book_by'); ?> <strong><?php echo userLink($book['offers'][0]['user_id']); ?></strong></span>
              </div>
              <div class="col-xs-4 text-right no-underline black-link">
                <span class="h3"><a href="<?php echo $book['offers'][0]['url']; ?>">&euro; <?php echo $book['offers'][0]['price']; ?></a></span>
              </div>
            </div>
          </div>
        </div>

      <?php } ?>
      <?php $bol = bol_url($book['isbnList'][0]['value']); if($bol){ ?>
        <a href="<?php echo $bol['url']; ?>" target="_blank" rel="nofollow">
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="row">
              <div class="col-xs-8">
                <strong class="text-primary"><?php echo lang('buy_new'); ?></strong><br>
                <span class="small text-muted"><?php echo lang('free_shipping'); ?> <strong>Bol.com</strong></span>
              </div>
              <div class="col-xs-4 text-right black-link">
                <span class="h3">&euro; <?php echo $bol['price']; ?></span>
              </div>
            </div>
          </div>
        </div>
        </a>
      <?php } ?>
        <div class="btn-group btn-group-justified" role="group" aria-label="...">
          <div class="btn-group" role="group">
            <a href="#offers" class="btn btn-primary"><?php echo lang('all_offers'); ?></a>
          </div>
          <div class="btn-group" role="group">
            <a href="<?php echo site_url('addbook/add/'.$book['id']); ?>" class="btn btn-default"><?php echo lang('buynsell'); ?></a>
          </div>
        </div>
    </div>
</div>
</span>
<hr>
<div class="row">
  <div class="col-md-12">

    <?php if(($book['offer_count'] + $book['request_count']) > 0){ ?>
        <div class="row">
          <div class="col-lg-12">
            <a name="offers"></a>
            <h3><?php echo lang('all_offers'); ?> <span class="badge"><?php echo $book['offer_count']; ?></span></h3>
          </div>
          <div class="col-lg-12">
            <?php if($book['offer_count'] > 0){ ?>
              <div class="list-group">
                <?php 
                    $current_url = currentPageUrl();
                    foreach($book['offers'] as $ad){ ?>
                      <span class="list-group-item no-underline" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                        <div class="row">
                          <div class="col-sm-6 col-md-8 col-xs-6">
                            <a href="<?php echo ($logged) ? $ad['url'] : site_url('register/'.base64_url_encode($current_url)); ?>"><h4 class="list-group-item-heading"><?php echo $ad['state']; ?></h4>
                            <p class="list-group-item-text"><?php echo ($ad['description'] == '') ? '<em class="text-muted">No description</em>' : '<em class="text-muted">'.$ad['description'].'</em>'; ?></p></a>
                          </div>
                          <div class="col-sm-4 col-md-2 col-xs-6">
                            <p class="list-group-item-text"><?php echo ($logged) ? userLink($ad['user_id']) : '<a href="'.site_url('register/'.base64_url_encode($current_url)).'" class="btn btn-primary btn-sm">Login</a> <a href="'.site_url('register/'.base64_url_encode($current_url)).'" class="btn btn-default btn-sm">Register</a>'; ?></p>
                          </div>
                          <div class="col-sm-2 col-md-2 col-xs-6"><span class="h3 text-default no-underline black-link"><a href="<?php echo ($logged) ? $ad['url'] : site_url('register/'.base64_url_encode($current_url)); ?>">&euro; <span itemprop="price"><?php echo $ad['price']; ?></span></a></span></div>
                          <meta itemprop="priceCurrency" content="EUR">
                          <meta itemprop="seller" content="<?php echo $ad['name']; ?>">
                          <meta itemprop="availability" content="InStock">
                        </div>
                      </span>
                <?php } ?>
                </div>
              <?php
              }else{
                ?><div class="alert alert-info"><strong><?php echo lang('sorry'); ?></strong><br/><?php echo lang('no_offers'); ?> <strong><?php echo $book['title']; ?></strong></div><?php
              }
              ?>
          </div>
        </div>
        
        <hr/>

        <div class="row">
            <div class="col-lg-12">
              <a name="offers"></a>
              <h3><?php echo lang('all_requests'); ?> <span class="badge"><?php echo $book['request_count']; ?></span></h3>
            </div>
            <div class="col-lg-12">
              <?php if($book['request_count'] > 0){ ?>
                <div class="list-group">
                  <?php 
                      $current_url = currentPageUrl();
                      foreach($book['requests'] as $ad){ ?>
                        <span class="list-group-item no-underline" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                          <div class="row">
                            <div class="col-sm-6 col-md-8 col-xs-6">
                              <a href="<?php echo ($logged) ? $ad['url'] : site_url('register/'.base64_url_encode($current_url)); ?>"><h4 class="list-group-item-heading"><?php echo $ad['state']; ?></h4>
                              <p class="list-group-item-text"><?php echo ($ad['description'] == '') ? '<em class="text-muted">No description</em>' : '<em class="text-muted">'.$ad['description'].'</em>'; ?></p></a>
                            </div>
                            <div class="col-sm-4 col-md-2 col-xs-6">
                              <p class="list-group-item-text"><?php echo ($logged) ? userLink($ad['user_id']) : '<a href="'.site_url('register/'.base64_url_encode($current_url)).'" class="btn btn-primary btn-sm">Login</a> <a href="'.site_url('register/'.base64_url_encode($current_url)).'" class="btn btn-default btn-sm">Register</a>'; ?></p>
                            </div>
                            <div class="col-sm-2 col-md-2 col-xs-6"><span class="h3 text-default no-underline black-link"><a href="<?php echo ($logged) ? $ad['url'] : site_url('register/'.base64_url_encode($current_url)); ?>">&euro; <span itemprop="price"><?php echo $ad['price']; ?></span></a></span></div>
                            <meta itemprop="priceCurrency" content="EUR">
                            <meta itemprop="seller" content="<?php echo $ad['name']; ?>">
                            <meta itemprop="availability" content="InStock">
                          </div>
                        </span>
                  <?php } ?>
                </div>
              <?php
              }else{
                ?><div class="alert alert-info"><strong><?php echo lang('sorry'); ?></strong><br/><?php echo lang('no_requests'); ?> <strong><?php echo $book['title']; ?></strong></div><?php
              }
              ?>
          </div>
        </div>
    <?php
      }else{
        ?><div class="alert alert-info"><strong><?php echo lang('sorry'); ?></strong><br/><?php echo lang('no_both'); ?> <strong><?php echo $book['title']; ?></strong></div><?php
      }
    ?>
  </div>
</div>