<div class="row marketing">

    <div class="col-sm-6 col-sm-offset-3">

        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-heading">
                <div class="panel-title">
                    <h4 style="margin-top:0;"><?php echo lang('place_ad_success'); ?></h4>
                </div>
            </div>

            <div style="padding-top:30px" class="panel-body">
            
            <p>
                <?php echo lang('place_ad_success_msg_before'); ?><a href="<?php echo $book['url']; ?>"><?php echo $book['title']; ?></a><?php echo lang('place_ad_success_msg_after'); ?>
                <a href="<?php echo $offer['url']; ?>"><?php echo lang('place_ad_success_link'); ?></a>
            </p>
            <p>
                <?php echo lang('place_ad_success_tip'); ?><br><br>
                <input type="text" class="form-control" onFocus="this.select()" autofocus value="<?php echo $offer['url']; ?>"><br>
                <iframe src="//www.facebook.com/plugins/share_button.php?href=<?php echo urlencode($offer['url']); ?>&amp;layout=button&amp;appId=823364584395054" scrolling="no" frameborder="0" style="position:relative; top:10px;border:none; overflow:hidden; height:30px; width:100px;" allowTransparency="true"></iframe>
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo $book['url']; ?>" data-via="tueboeken" data-text="<?php echo $book['title']; ?>" data-count="none" data-hashtags="universiteitboeken">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
            </p>
            </div>
        </div>
    </div>
</div>