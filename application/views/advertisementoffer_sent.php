<div class="row marketing">

    <div class="col-sm-6 col-sm-offset-3">

        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-heading">
                <div class="panel-title"><h4 style="margin-top: 0;"><?php echo lang('ad_sent_thanks_1'); ?></h4></div>
            </div>

            <div style="padding-top:30px" class="panel-body">
            
            <p><?php echo lang('ad_sent_thanks_2'); ?></p>
            </div>
        </div>
    </div>
</div>