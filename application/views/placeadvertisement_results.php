<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="panel panel-default" style="margin-top: 20px;">
            <div class="panel-heading">
                <div class="panel-title"><h4 style="margin: 0;"><?php echo lang('add_step_1'); ?></h4></div>
            </div>

            <div class="panel-body">
            
            <?php echo form_open('addbook',array('class'=>'form-horizontal')); ?>
            <div class="input-group">
            <input type="text" value="<?php echo $query; ?>" name="add_searchQuery" class="form-control" placeholder="<?php echo lang('search_text'); ?>"/>
            <span class="input-group-btn">
              <button class="btn btn-primary" type="submit"><?php echo lang('search_button'); ?></button>
            </span>
            </div><!-- /input-group -->
            </form>

            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-sm-12"> 
        <?php if($query != '') { ?>
            <?php if($result_rows > 0) { ?>

                <center><em class="text-muted" style="font-size: 12px; margin-left: 10px;"><?php echo lang('search_add_found_1'); ?>: <?php echo $result_rows; ?> <?php echo lang('search_add_found_2'); ?> <a href="<?php echo site_url('addbook/add/0/'); ?>"><?php echo lang('search_add_found_3'); ?></a>!</center><?php /* on <?php echo $pages; ?> page(s) */ ?></em>
                <hr>

                <div class="row"><?php 
                    $count = 0;
                    foreach($results as $book) {
                        if($count%2 == 0) echo '</div><div class="row">';

                        if(isset($book['id'])){
                            $uO = site_url('addbook/add/'.$book['id'].'/?offer');
                            $uR = site_url('addbook/add/'.$book['id'].'/?request');
                        }else{
                            $uO = site_url('addbook/api/offer/'.$book['bolid']);
                            $uR = site_url('addbook/api/request/'.$book['bolid']);
                        }

                        ?>
                        <div class="col-sm-6">
                            <div class="well well-default">
                                <div class="row">

                                    <div class="col-sm-4">
                                        <a href="<?php echo $uO; ?>" target="_blank"><img class="book-thumb img-responsive" src="<?php echo $book['image_url']; ?>"></a>
                                    </div>

                                    <div class="col-sm-6">
                                        <h4 class="title no-underline black-link"><a href="<?php echo $uO; ?>" target="_blank"><?php echo $book['title']; ?></a></h4>
                                        <h6 class="subtitle"><?php echo $book['subtitle']; ?></h6>
                                        <div class="desc">
                                            <?php echo lang('author'); ?>: <?php echo $book['author']; echo '<br/>'; foreach($book['isbnlist'] as $isbn) { ?>
                                            <?php echo $isbn['name']; ?>: <?php echo $isbn['value']; ?><?php } ?>
                                        </div>
                                        <hr/>
                                        <p><a href="<?php echo $uO; ?>" class="btn btn-primary btn-xs"><?php echo lang('offers_action'); ?></a>
                                        <a href="<?php echo $uR; ?>" class="btn btn-primary btn-xs"><?php echo lang('requests_action'); ?></a></p>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php
                        $count++;
                    } 

                ?></div>

            <?php } else { ?>

    			<hr>
    			     <p><?php echo lang('no_books_found'); ?></p>
    			<hr>

            <?php } ?>

        <div class="alert alert-info" role="alert"><strong><?php echo lang('not_found_question_1'); ?></strong><br/>
            <?php echo lang('not_found_question_2'); ?><br/><br/><a href="<?php echo site_url('addbook/manually/'); ?>" class="btn btn-info btn-sm"><?php echo lang('not_found_question_3'); ?></a></div>

        <?php } ?>
    </div>
</div>

<?php /*
            <ul class="pagination">
                <li class="disabled"><a>«</a>
                </li>
                <li class="active"><a href="http://tueboeken.nl/search/1/?q=calculus">1</a>
                </li>
                <li><a href="http://tueboeken.nl/search/2/?q=calculus">2</a>
                </li>
                <li><a href="http://tueboeken.nl/search/3/?q=calculus">3</a>
                </li>
                <li><a href="http://tueboeken.nl/search/2/?q=calculus">»</a>
                </li>
            </ul>
*/ ?>
