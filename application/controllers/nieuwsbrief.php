<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nieuwsbrief extends CI_Controller {

	public $headerData = array();

	public function uitgave($maand){

		$this->headerData['EXTRA_HEADER'] .= '
			<style type="text/css">
				#search-etc, .footer-holder, #user{
					display: none;
				}

				body{
					background-color: #fff;
				}

				p{
					color: #222;
				}
			</style>
		';

		$this->headerData['TITLE'] = 'Nieuwsbrief - '.lang('site_name');
		$this->load->view('template/header', $this->headerData);
		$this->load->view('nieuwsbrief/'.$maand);
		$this->load->view('template/footer');
	}
}