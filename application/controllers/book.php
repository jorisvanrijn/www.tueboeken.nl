<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Book extends CI_Controller {

	public $headerData = array();

	function __construct(){
		parent::__construct();
        $this->lang->load('book');
        $this->lang->load('book_state');
        $this->load->model('user','',TRUE);
        $this->load->model('bookinfo','',TRUE);
        $this->load->model('bookList','',TRUE);
        $this->load->model('advertisementlist','',TRUE);

        $this->load->helper('bol');

		$this->headerData['active'] = '';
		$this->headerData['EXTRA_HEADER'] = '';

		if($this->user->checkLogin()){
		  // user exists
		  $this->headerData['user'] = $this->user->checkLogin();
		  $this->headerData['logged_in'] = true;
		} else {
		  $this->headerData['logged_in'] = false;
		}
	}

    public function delete($id, $a = ''){
        $offerRes = $this->db->get_where('advertisement', array('id'=>$id));
        $data['offer'] = $offerRes->row_array();

        if($a == 'sold' OR $a == 'now'){
          $status = 2;          // removed status
          if($a == 'sold'){
            $status = 4;        // sold status
          }

          $this->db->update('advertisement', array('status_id' => $status), array('id' => $id));

          redirect('profile', 'refresh');
        }

        $this->headerData['TITLE'] = 'Delete profile' . lang('title_seperator') . lang('site_name');
        $this->load->view('template/header', $this->headerData);
        $this->load->view('advertisement_delete', $data);
        $this->load->view('template/footer');
    }

	public function index($letter = '') // overview
	{
        $bookData = array();

        $bookData['letter'] = '';
        if($letter == ''){
            $bookData['books'] = $this->bookList->get(array(), null, null, 'title ASC');
        }else{
            $q = 'SELECT * FROM `book` WHERE `title` LIKE "'.$this->db->escape_like_str($letter).'%" ORDER BY title ASC';
            $this->bookList->customQuery($q);
            $bookData['books'] = $this->bookList->get();
            $bookData['letter'] = $letter;
        }

		$this->headerData['active'] = 'book';
		$this->headerData['TITLE'] = lang('book_overview_title') . lang('title_seperator') . lang('site_name');
        $this->load->view('template/header',$this->headerData);
		$this->load->view('books',$bookData);
		$this->load->view('template/footer');
	}


	public function view($book){
        $bookInfo = $this->bookinfo->get(array('slug'=>$book),1);

        if(is_array($bookInfo)){            
            $adInfo = array();
            $adInfo['book'] = $bookInfo;

            $adInfo['book']['offer_count'] = $this->bookinfo->advertisements_count(1, array(1, 3));
            $adInfo['book']['request_count'] = $this->bookinfo->advertisements_count(0, array(1, 3));

            $adInfo['book']['requests'] = $this->bookinfo->advertisements(0, array(1, 3), 'price ASC');
            $adInfo['book']['offers'] = $this->bookinfo->advertisements(1, array(1, 3), 'price ASC');

            $adInfo['book']['isbnList'] = $this->bookinfo->isbnList();
            $adInfo['book']['courseList'] = $this->bookinfo->courseList();
            $adInfo['logged'] = $this->user->checkLogin();

            $this->headerData['TITLE'] = $bookInfo['title'] . lang('title_seperator') . lang('site_name');

            $this->load->view('template/header',$this->headerData);
            $this->load->view('advertisement',$adInfo);
            $this->load->view('template/footer');
        } else {
            redirect('404');
        }
	}

    public function replysuccess(){
        $this->headerData['TITLE'] = $resData['query'] . lang('title_seperator') . lang('site_name');
        $this->load->view('template/header',$this->headerData);
        $this->load->view('advertisementoffer_sent');
        $this->load->view('template/footer');
    }

    public function viewOffer($book,$offerId,$mode=''){
        $this->load->library('form_validation');

        $showLayout = true;

        $bookInfo = $this->bookinfo->get(array('slug'=>$book),1);

        if(is_array($bookInfo)){

            $this->db->where_in('status_id',array(1,3)); // only books that are for sale or reserved
            $offerRes = $this->db->get_where('advertisement', array('id'=>$offerId, 'book_id'=>$bookInfo['id']));

            if($offerRes->num_rows() == 1){

                $adInfo = array();

                $adInfo['offer'] = $offerRes->row_array();

                $adInfo['book'] = $bookInfo;
                $adInfo['book']['isbnList'] = $this->bookinfo->isbnList();
                $adInfo['book']['advertisements_count'] = $this->bookinfo->advertisements_count(1, array(1, 3)) - 1;

                // price
                setlocale(LC_MONETARY, 'en_US');
                $adInfo['offer']['price'] = money_format('%i', $adInfo['offer']['price']);

                $this->db->select('email, name');
                $userRes = $this->db->get_where('user', array('id'=>$adInfo['offer']['user_id']));
                $user = $userRes->row_array();

                $adInfo['offer']['user'] = $user['name'];
                $adInfo['offer']['user_email'] = $user['email'];
                $adInfo['offer']['user_url'] = userLink($adInfo['offer']['user_id']);

                $adInfo['bookUrl'] = base_url('book/'.$book);
                $adInfo['offerUrl'] = base_url('book/'.$book.'/'.$offerId);
                if($this->headerData['logged_in'] && ($this->headerData['user']['id'] == $adInfo['offer']['user_id'])){
                    $adInfo['editUrl'] = base_url('book/'.$book.'/'.$offerId.'/edit');
                }


                if(($mode == 'edit') && $this->headerData['logged_in'] && ($this->headerData['user']['id'] == $adInfo['offer']['user_id'])){
                    $adInfo['editMode'] = 1;

                    $this->db->order_by('cOrder ASC');
                    $q = $this->db->get('state');
                    $a = $q->result_array();
                    $sArray = array();
                    foreach($a as $s){
                        $sArray[$s['id']] = lang($s['languagekey']);
                    }
                    $adInfo['status'] = $sArray;
                    
                    $this->load->helper('form');
                    $this->load->library('form_validation');

                    $this->form_validation->set_rules('price', 'lang:book_price', 'trim|required|greater_than[0]|less_than[300]');
                    //$this->form_validation->set_rules('state', 'lang:book_state', 'trim|required|integer');
                    $this->form_validation->set_rules('description', 'lang:book_description', 'required');

                    if($this->form_validation->run()){
                        $updateAd = array();
                        $updateAd['price'] = $this->input->post('price');
                        $updateAd['state_id'] = $this->input->post('state');
                        $updateAd['description'] = $this->input->post('description');
                        $this->db->where('id', $adInfo['offer']['id']);
                        $this->db->update('advertisement',$updateAd);
                        redirect($adInfo['offerUrl'], 'location', 307);
                    }

                } else {
                    $adInfo['editMode'] = 0;
                }





                if($this->headerData['logged_in'] && $this->input->post('reply_message')){

                    $this->form_validation->set_rules('reply_message', 'lang:book_respond_message', 'required|min_length[6]');
                    if ($this->form_validation->run()){
                        $reply  = array();
                        $reply['site_id'] = $this->config->item('site_id');
                        $reply['advertisement_id'] = $adInfo['offer']['id'];
                        $reply['user_id'] = $this->headerData['user']['id'];
                        $reply['seller_user_id'] = $adInfo['offer']['user_id'];
                        $reply['message'] = $this->input->post('reply_message');

                        $this->db->insert('reply',$reply);


                        $find = array('[name]',
                                        '[email]',
                                        '[buyer]',
                                        '[phone]',
                                        '[message]',
                                        '[url]',
                                        '[title]');
                        $replace = array($adInfo['offer']['user'],
                                            $this->headerData['user']['email'], 
                                            $this->headerData['user']['name'], 
                                            $this->headerData['user']['phone'], 
                                            $reply['message'], 
                                            current_url(),
                                            $adInfo['book']['title']);

                        // email seller
                        $this->load->library('email');
                        $this->email->from(lang('settings_contact_from'), $this->headerData['user']['name']);
                        $this->email->to($adInfo['offer']['user_email']);

                        $this->email->subject(str_replace($find,$replace,lang('book_reply_subject')));
                        $this->email->message(str_replace($find,$replace,lang('book_reply_message'))); 

                        $this->email->send();

                        // email copy to buyer
                        $this->email->from(lang('settings_contact_from'), lang('settings_contact_from_name'));
                        $this->email->to($this->headerData['user']['email']);

                        $this->email->subject(str_replace($find,$replace,lang('book_reply_c_subject')));
                        $this->email->message(str_replace($find,$replace,lang('book_reply_c_message'))); 

                        $this->email->send();

                        $this->headerData['TITLE'] = $resData['query'] . lang('title_seperator') . lang('site_name');
                        $this->load->view('template/header',$this->headerData);
                        $this->load->view('advertisementoffer_sent');
                        $this->load->view('template/footer');

                        $showLayout = false;

                    }
                }

                // get more offers
                $offerList = $this->advertisementlist->get(array('id !='=>$adInfo['offer']['id'],'user_id'=>$adInfo['offer']['user_id']),array(1,3),'created ASC');
                if($offerList){
                    $adInfo['offerList'] = $offerList;
                    $adInfo['moreOffers'] = true;
                } else{
                    $adInfo['moreOffers'] = false;
                }
                // get state
                $this->db->select('languagekey');
                $stateRes = $this->db->get_where('state',array('id'=>$adInfo['offer']['state_id']));
                $state = $stateRes->row_array();
                $adInfo['offer']['state'] = lang($state['languagekey']);
                $adInfo['header'] = $this->headerData;

                if($showLayout){
                    $this->headerData['TITLE'] = $adInfo['offer']['state'] . lang('title_seperator') . $bookInfo['title'] . lang('title_seperator') . lang('site_name');

                    $this->load->view('template/header',$this->headerData);
                    $this->load->view('advertisementoffer',$adInfo);
                    $this->load->view('template/footer');
                }

            } else {
                redirect(base_url('/book/'.$book), 'location', 301);
            }

            
        } else {
            redirect('404');
        }
    }

    public function offersByUser($userId){
        $this->db->select('name, email, courses_id');
        $userRes = $this->db->get_where('user', array('id'=>$userId));
        
        if($userRes->num_rows() > 0){
            $user = $userRes->row_array();

            $adList = $this->advertisementlist->get(array('user_id'=>$user['id']),array(1,3),'created ASC');

            $adInfo = array();
            $adInfo['user'] = $user;
            $adInfo['advertisements'] = $adList;

            $this->load->view('template/header',$this->headerData);
            $this->load->view('advertisementuser',$adInfo);
            $this->load->view('template/footer');
        } else {
            redirect('404');
        }
    }

    public function search($query, $page=0){

        $this->load->model('booklist','',TRUE);

        // save term to db:

        if($this->headerData['logged_in']){
            $id = $this->headerData['user']['id'];
        }else{
            $id = -1;
        }

        $this->db->insert('searches', array('user_id'=>$id, 'search'=>$query));

        $resData = array();
        $resData['query'] = htmlspecialchars(urldecode($query));

        $this->booklist->search($query);

        if(is_numeric($page)){
            $this->booklist->slice_results(15,$page);
        }

        $results = $this->booklist->get();
        if(!$results){
            $resData['result_rows'] = 0;
        } else {
            $resData['results'] = $results;
            $resData['courseList'] = $this->booklist->courseCodes;
            $resData['result_rows'] = $this->booklist->rows;

            $this->load->library('pagination');
            $config['base_url'] = base_url('/search/'.$query);
            $config['total_rows'] = $resData['result_rows'];
            $config['per_page'] = 15; 
            $this->pagination->initialize($config); 

            $resData['page_numbers'] = $this->pagination->create_links();

        }

        

        $this->headerData['TITLE'] = $resData['query'] . lang('title_seperator') . lang('site_name');
        $this->load->view('template/header',$this->headerData);
        $this->load->view('results', $resData);
        $this->load->view('template/footer');
    }

}