<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Redirect extends CI_Controller {

    public function book($all){
        redirect('book/'.$all,'location',301);
    }

    public function over(){
        redirect('about','location',301);
    }

    public function home(){
        redirect(site_url(),'location',301);
    }

    public function addbook(){
        redirect('addbook','location',301);
    }


}