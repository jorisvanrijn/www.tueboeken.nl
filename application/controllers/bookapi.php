<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bookapi extends CI_Controller {

    function __construct(){
        parent::__construct();
    }

	public function getBook($q){


		$bookData = array();


        $searchQuery = "SELECT id, title, slug, MATCH(`title`,`subtitle`,`author`,`description`)
      
        AGAINST('".$this->db->escape_like_str($q)."') AS SCORE FROM book

        WHERE (book.id IN (

          SELECT advertisement.book_id FROM advertisement WHERE advertisement.user_id IN (SELECT user.id FROM user WHERE user.name LIKE '%".$this->db->escape_like_str($q)."%')

        )

        OR

        MATCH(`title`,`subtitle`,`author`,`description`) AGAINST('".$this->db->escape_like_str($q)."')

        OR

        book.id IN (

          SELECT tue_book_course.book_id FROM tue_book_course WHERE tue_book_course.code LIKE '%".$this->db->escape_like_str($q)."%'

        )

        OR

        book.id IN (

          SELECT book_isbn.book_id FROM book_isbn WHERE book_isbn.value LIKE '%".$this->db->escape_like_str($q)."%'

        ))

        AND site_id='".$this->config->item('site_id')."'

        ORDER BY SCORE DESC";

        $bookRes = $this->db->query($searchQuery);

        if($bookRes->num_rows() > 0) { // make sure there is at least one result

        	$bookDB = $bookRes->result_array();

        	foreach($bookDB as $book){
    			$bookData[$i] = array();
				$bookData[$i]['site_id'] = $book['id'];
				$bookData[$i]['title'] = $book['title'];
				$bookData[$i]['link'] = base_url('book/'.$book['slug']);
        	}

        }

        if($bookRes->num_rows() < 5) { // not enough results, search API's

			$url = 'https://api.bol.com/catalog/v4/search?apikey=7066B13FF7DA4CD796BC6D8BB4511906&format=json&ids=8299&q=';
		
			$url = $url.$q;
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HEADER, false);
			$data = curl_exec($curl);
			curl_close($curl);

			$data = json_decode($data,true);

			if($data['totalResultSize'] == 3){ 													//!!!! weer terug naar != 0 doen!!!!!!! dit is om google api te testen

				$i = 0;

				foreach($data['products'] as $product){
					$bookData[$i] = array();
					$bookData[$i]['site_id'] = 0;
					$bookData[$i]['title'] = isset($product['title']) ? $product['title'] : '';
					$bookData[$i]['subtitle'] = isset($product['subtitle']) ? $product['subtitle'] : '';
					$bookData[$i]['author'] = isset($product['specsTag']) ? $product['specsTag'] : '';
					$bookData[$i]['description'] = isset($product['longDescription']) ? : '';
					$bookData[$i]['image'] = isset(end($product['images'])['url']) ? end($product['images'])['url'] : '';
					//publisher?

					$bookData[$i]['isbnList'] = array();
					$bookData[$i]['isbnList']['ean'] = $product['ean'];

					$bookData[$i]['bolid'] = $product['id'];

					//var_dump($product);
					//print('<hr>');

					$i++;
				}


			//not found at Bol.com, then search at Google Books
		    } else {
		    	$url = 'https://www.googleapis.com/books/v1/volumes?key=AIzaSyClXkpyRbH9VNE49xQ9_PGGUsKrkpbskUc&q=';

		    	$url = $url.$isbn;
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $url);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_HEADER, false);
				$dataGoogle = curl_exec($curl);
				curl_close($curl);

				$dataGoogle = json_decode($dataGoogle,true);

				if($data['totalItems'] != 0){ 													//!!!! weer terug naar != 0 doen!!!!!!! dit is om google api te testen

					$i = 0;

					foreach($data['items'] as $product){
						$bookData[$i] = array();
						$bookData[$i]['site_id'] = 0;
						$bookData[$i]['title'] = isset($product['volumeInfo']['title']) ? $product['title'] : '';
						$bookData[$i]['subtitle'] = '';
						$bookData[$i]['author'] = (count($product['volumeInfo']['authors']) > 0) ? implode(', ', $product['volumeInfo']['authors']) : '';
						$bookData[$i]['description'] = isset($product['volumeInfo']['description']) ? : '';
						// $bookData[$i]['image'] = isset(end($product['images'])['url']) ? end($product['images'])['url'] : '';		nog even uitvogelen hoe image te pakken
						//publisher?

						//hieronder nog niet ge-google-bookt, is nog zelfde als Bol
						$bookData[$i]['isbnList'] = array();
						$bookData[$i]['isbnList']['ean'] = $product['ean'];

						$bookData[$i]['bolid'] = $product['id'];

						//var_dump($product);
						//print('<hr>');

						$i++;
					}


    		    }

    		}

        }

		$this->output
	    	->set_content_type('application/json')
	    	->set_output(stripslashes(json_encode($dataGoogle)));
	    	//->set_output(stripslashes(json_encode($bookData)));
    }


}