<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

  public $headerData;

  function __construct(){
    parent::__construct();
    $this->lang->load('profile');
    $this->load->model('user','',TRUE);
  	$this->load->model('advertisementlist','',TRUE);
  	$this->load->model('reply','',TRUE);
  	$this->load->model('replyholder','',TRUE);

    $this->headerData['active'] = '';
    $this->headerData['EXTRA_HEADER'] = '';

    if($this->user->checkLogin()){
      $this->headerData['user'] = $this->user->checkLogin();
      $this->headerData['logged_in'] = true;
    } else {
      redirect('home/login');
    }
  }

  function index($id = ''){

    $profileData = array(); 

    $ownProfile = false;
    if($id == ''){
      $id = $this->headerData['user']['id'];
      $ownProfile = true;
    }else{
      if($id == $this->headerData['user']['id']){
        redirect('profile');
      }
    }

    $profileData['ownProfile'] = $ownProfile;

  	if(!$this->session->userdata('img')){
  		$this->session->set_userdata('img', base_url('theme/img/round-icons/free-60-icons-'.str_pad(rand(0,60), 2, "0", STR_PAD_LEFT).'.png'));
  	}  

  	$offerList = $this->advertisementlist->get(array('user_id'=>$id),array(1,3),'created ASC');
  	$profileData['offerList'] = $offerList;
  	
  	// Sold books	
  	if(($x = $this->advertisementlist->get(array('user_id'=>$id),array(4),'created ASC'))){
  		$profileData['stat']['sold'] = count($x);
  	}else{
  		$profileData['stat']['sold'] = 0;
  	}
  	
  	// Offered
  	if(($x = $this->advertisementlist->get(array('isforsale' => 1, 'user_id'=>$id),array(1,3),'created ASC'))){
  		$profileData['stat']['offered'] = count($x);
  	}else{
  		$profileData['stat']['offered'] = 0;
  	}
  	
  	// Requested
  	if(($x = $this->advertisementlist->get(array('isforsale' => 0, 'user_id'=>$id),array(1,3),'created ASC'))){
  		$profileData['stat']['requested'] = count($x);
  	}else{
  		$profileData['stat']['requested'] = 0;
  	}
  	
  	// Replies
  	$this->replyholder->getRepliesFromUser($id);
  	$profileData['replies'] = $this->replyholder->getReplies();
  	
  	$profileData['user'] = $this->headerData['user'];
  	
    $q = $this->db->get_where('user', array('id' => $id));
  	$profileData['currentUser'] = $q->row_array();
	
    if($ownProfile){
      $t = lang('profile_view_title');
    }else{
      $t = $profileData['currentUser']['name'];
    }

    $this->headerData['TITLE'] = $t . lang('title_seperator') . lang('site_name');
    $this->load->view('template/header', $this->headerData);
    $this->load->view('profile', $profileData);
    $this->load->view('template/footer');
  }

  function delete($a = ''){
    if($a == 'now'){
      $this->db->delete('user', array('id' => $this->headerData['user']['id']));
      $this->db->delete('advertisement', array('user_id' => $this->headerData['user']['id']));
      $this->db->delete('reply', array('user_id' => $this->headerData['user']['id']));
      $this->db->delete('reply', array('seller_user_id' => $this->headerData['user']['id']));
       
      $this->session->unset_userdata('logged_in');
      $this->session->unset_userdata('img');
      redirect('home', 'refresh');
    }

    $this->headerData['TITLE'] = 'Delete profile' . lang('title_seperator') . lang('site_name');
    $this->load->view('template/header', $this->headerData);
    $this->load->view('profile_delete');
    $this->load->view('template/footer');
  }

  function ads(){
    $this->headerData['TITLE'] = lang('profile_ads_title') . lang('title_seperator') . lang('site_name');
    $this->load->view('template/header', $this->headerData);
    $this->load->view('profile');
    $this->load->view('template/footer');
  }

  function logout(){
    $this->session->unset_userdata('logged_in');
	  $this->session->unset_userdata('img');
    redirect('home', 'refresh');
  }

  function edit(){
    $this->lang->load('home');
    $editData = array();
    $this->headerData['TITLE'] = lang('profile_change_title') . lang('title_seperator') . lang('site_name');
    $editData['msg'] = '';

    if($_SERVER['REQUEST_METHOD'] == 'POST'){

      $startyear = ($this->input->post('startyear') >= 1970 && $this->input->post('startyear') <= (date('Y') + 1))? $this->input->post('startyear') : 0;
      $courses_id = (is_numeric($this->input->post('courses_id')))? $this->input->post('courses_id') : 0;

      if( $this->input->post('name')) {
        $res = $this->db->update('user', array(
          'name' => htmlspecialchars($this->input->post('name')),
          'courses_id' => $courses_id,
          'email' => htmlspecialchars($this->input->post('email')),
          'phone' => $this->input->post('tel'),
          'startyear' => $startyear,
          'description' => htmlspecialchars($this->input->post('description')),
        ), array('id' => $this->headerData['user']['id']));

        if($res){
          $editData['msg'] = '<hr/><div class="alert alert-success"><strong>'.lang('profile_changed_title').'</strong><br/>'.lang('profile_changed_message').'</div>';

          /* Opnieuw data ophalen om verwarring voor de gebruiker te voorkomen */
          $this->headerData['user'] = $this->user->checkLogin();
        }else{
          $editData['msg'] = '<hr/><div class="alert alert-danger"><strong>'.lang('profile_not_changed_title').'</strong><br/>'.lang('profile_not_changed_message').'</div>';
        }
      }elseif( $this->input->post('old_password')) {

        if($this->headerData['user']['password'] == salt_password($this->input->post('old_password'))){
          

          if( $this->input->post('password_1') == $this->input->post('password_2') ){

            $res = $this->db->update('user', array(
              'password' => salt_password($this->input->post('password_1')),
            ), array('id' => $this->headerData['user']['id']));

            if($res){
              $editData['msg'] = '<hr/><div class="alert alert-success"><strong>'.lang('password_changed_title').'</strong><br/>'.lang('password_changed_message').'</div>';            
            }else{
              $editData['msg'] = '<hr/><div class="alert alert-danger"><strong>'.lang('password_not_changed_title').'</strong><br/>'.lang('password_not_changed_message').'</div>';
            }

          }else{

            $editData['msg'] = '<hr/><div class="alert alert-danger"><strong>'.lang('password_not_changed_title').'</strong><br/>'.lang('password_not_changed_message_new_password').'</div>';            

          }


        }else{

          $editData['msg'] = '<hr/><div class="alert alert-danger"><strong>'.lang('password_not_changed_title').'</strong><br/>'.lang('password_not_changed_message_old_password').'</div>';

        }

      }

    }

    $this->load->view('template/header', $this->headerData);
    $this->load->view('profile_edit',$editData);
    $this->load->view('template/footer');
  }

}