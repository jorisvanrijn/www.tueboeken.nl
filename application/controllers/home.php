<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public $headerData = array();

	function __construct(){
		parent::__construct();
		$this->lang->load('home');
		$this->load->model('user','',TRUE);
		$this->load->helper('general','',TRUE);

		$this->headerData['active'] = '';
		$this->headerData['EXTRA_HEADER'] = '';

		if($this->user->checkLogin()){
			print_r($this->user->checkLogin(),true);
			$this->headerData['logged_in'] = true;
			print_r($this->headerData,true);
			$this->headerData['user'] = $this->user->checkLogin();
		} else {
			$this->headerData['logged_in'] = false;
		}
	}

	public function index()
	{

		if($this->input->post('searchQuery')){
			redirect('search/'.$this->input->post('searchQuery'), 'location' ,307);
		} else {

			$this->load->model('bookList','',TRUE);

			$this->headerData['active'] = 'home';
			$this->headerData['TITLE'] = lang('site_name').' - '.lang('site_title');

			$homeData = array();

			// number of books
			$this->db->select('id');
			$bookRes = $this->db->get_where('book',array('site_id'=>$this->config->item('site_id')));
			$homeData['numberOfBooks'] = $bookRes->num_rows();

			// number of advertisements
			$this->db->select('id');
			$adRes = $this->db->get_where('advertisement',array('site_id'=>$this->config->item('site_id')));
			$homeData['numberOfAdvertisements'] = $adRes->num_rows();

			$this->db->select('id');
			$adRes = $this->db->get_where('user',array('site_id'=>$this->config->item('site_id')));
			$homeData['numberOfStudents'] = $adRes->num_rows();

			// top two books
			$this->bookList->noSearch();
			//$this->bookList->customQuery("SELECT * FROM (`advertisement`) WHERE `status_id` IN (1, 3) AND `site_id` = 1 AND `book_id` = '114' AND `isforsale` = 0");
			//$homeData['books'] - $this->bookList->get();
			$d = $this->bookList->get(array('description <>' => ''), 20, 0,'id DESC');
			//var_dump($this->db->last_query());

			$counts = array();
			foreach($d as $k => $b){
				$counts[$k] = $b['offered']+$b['requested'];
			}

			$homeData['books'] = array();
			arsort($counts);
			$newArray = array_keys($counts);

			//var_dump($counts);

			for ($i = 0; $i < 8; $i++) { 
				$homeData['books'][] = $d[$newArray[$i]];
			}

			$homeData['logged'] = $this->user->checkLogin();

			$this->load->view('template/header', $this->headerData);
			$this->load->view('homepage',array_merge($homeData, $this->headerData));
			$this->load->view('template/footer');

		}
	}

	public function login($o=''){
		$this->headerData['active'] = 'login';
		$this->headerData['TITLE'] = lang('home_login_title').' - '.lang('site_name');

        if($this->headerData['logged_in']){
            redirect('profile', 'location', 307);
        }

		$loginData = array();
		$loginData['page'] = '';
		$loginData['showRegisterErrors'] = false;

		if($o == 'verify' AND $_SERVER['REQUEST_METHOD'] == 'POST'){
		   // verify login
		   $loginData['page'] = 'login';

		   $this->load->library('form_validation');

		   $this->form_validation->set_rules('login_email', 'lang:home_login_email', 'trim|required|xss_clean|valid_email');
		   $this->form_validation->set_rules('login_password', 'lang:home_login_password', 'trim|required|xss_clean|callback_check_database');

		   if($this->form_validation->run() != FALSE)
		   {
			    if($this->session->userdata('refreshTo')){
					//var_dump($this->session->userdata('refreshTo')); die();
					$url = $this->session->userdata('refreshTo');
					$this->session->unset_userdata('refreshTo');
		    		redirect('http://'.$url, 'refresh');
		    	}else{
		    		redirect('profile', 'refresh');
		    	}
		   }
		}

		$this->load->helper('form');

		$this->load->view('template/header', $this->headerData);
		$this->load->view('login', $loginData);
		$this->load->view('template/footer');
	}

	public function register($o=''){

		if($o != 'verify' AND $o != ''){
			$this->session->set_userdata('refreshTo', base64_url_decode($o));
		}

		$loginData = array();
		$loginData['page'] = '';
		$loginData['showRegisterErrors'] = false;

        if($this->headerData['logged_in']){
            redirect('profile', 'location', 307);
        }

		if($o == 'verify' AND $_SERVER['REQUEST_METHOD'] == 'POST'){
		   // register
		   $loginData['page'] = 'register';
		   $loginData['showRegisterErrors'] = true;

		   $this->load->library('form_validation');

		   $this->form_validation->set_rules('name', 'lang:home_login_name', 'trim|required');
		   $this->form_validation->set_rules('email', 'lang:home_login_email', 'trim|required|xss_clean|is_unique[user.email]|valid_email');
		   $this->form_validation->set_rules('password', 'lang:home_login_password', 'required|min_length[5]');
		   $this->form_validation->set_rules('repeat', 'lang:home_login_repeat', 'required|xss_clean|matches[password]');

		   $this->form_validation->set_message('is_unique', lang('home_login_email_exists'));

		   if($this->form_validation->run() != FALSE)
		   {
		    	//register our new user :-)
		   		$this->user->register($this->input->post('name'),$this->input->post('password'),$this->input->post('email'));

				// login the user
				if($this->check_database($this->input->post('password'))){

					if($this->session->userdata('refreshTo')){
						$this->session->unset_userdata('refreshTo');
			    		redirect($this->session->userdata('refreshTo'));
			    	}else{
			    		redirect('profile', 'refresh');
			    	}

			    } else {
			    	// something went wrong with login, just refresh to try again
			    	redirect('home/register', 'refresh');
			    }
		   }
		}

		$this->headerData['active'] = 'register';
		$this->headerData['TITLE'] = lang('home_register_title').' - '.lang('site_name');

		$this->load->helper('form');

		$this->load->view('template/header', $this->headerData);
		$this->load->view('register', $loginData);
		$this->load->view('template/footer');
	}


	 function check_database($password){
	   //Field validation succeeded.  Validate against database
	   $username = $this->input->post('login_email');

	   //query the database
	   $result = $this->user->login($username, $password);

	   if($result)
	   {
	       $sess_array = array(
	         'uuid' => $result['password'],
	         'email' => $result['email']
	       );
	       $this->session->set_userdata('logged_in', $sess_array);

	    	// big data
	   		$this->bigdata->add('login',$result['id']);

	     return TRUE;
	   }
	   else
	   {
	     $this->form_validation->set_message('check_database', lang('home_login_error','alert alert-danger',true));
	     return false;
	   }
	}

	public function forgot(){

        $this->headerData['active'] = 'home/login';
        $this->headerData['TITLE'] = lang('home_forgot_title').' - '.lang('site_name');
        
        $this->load->view('template/header', $this->headerData);

        $this->load->library('form_validation');
        $this->form_validation->set_rules('forgot_email', 'lang:home_login_email', 'trim|required|xss_clean');

        if($this->form_validation->run() != FALSE){
            $this->user->resetPassword($this->input->post('forgot_email'));
            $this->load->view('login_forgot_sent');
        } else {
            $this->load->view('login_forgot');
        }


        $this->load->view('template/footer');
	}

	public function contact(){
		$this->headerData['TITLE'] = lang('home_contact_title').' - '.lang('site_name');
		$this->load->view('template/header', $this->headerData);
		$this->load->view('contact');
		$this->load->view('template/footer');
	}

	public function about(){
		$this->headerData['TITLE'] = lang('home_about_title').' - '.lang('site_name');
		$this->load->view('template/header', $this->headerData);
		$this->load->view('about');
		$this->load->view('template/footer');
	}

	public function terms(){
		$this->headerData['TITLE'] = lang('home_about_title').' - '.lang('site_name');
		$this->load->view('template/header', $this->headerData);
		$this->load->view('termsconditions');
		$this->load->view('template/footer');
	}

	public function sitemap(){
		$this->headerData['TITLE'] = 'Sitemap - '.lang('site_name');
		$this->load->view('template/header', $this->headerData);
		$this->load->view('sitemap');
		$this->load->view('template/footer');
	}

	public function notfound(){
		header("HTTP/1.0 404 Not Found");
		$this->headerData['TITLE'] = '404 Not Found - '.lang('site_name');
		$this->load->view('template/header', $this->headerData);
		$this->load->view('fourohfour');
		$this->load->view('template/footer');
	}

    public function replysuccess(){
        $this->headerData['TITLE'] = 'Success!' . lang('title_seperator') . lang('site_name');
        $this->load->view('template/header',$this->headerData);
        $this->load->view('advertisementoffer_sent');
        $this->load->view('template/footer');
    }
}