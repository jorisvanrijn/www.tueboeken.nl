<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Addbook extends CI_Controller {

    public $headerData = array();

    function __construct(){
        parent::__construct();
        $this->lang->load('book');
        $this->lang->load('home');
        $this->load->model('user','',TRUE);
        $this->load->model('booklist','',TRUE);
        $this->load->helper('string');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->headerData['active'] = 'addbook';
        $this->headerData['EXTRA_HEADER'] = '';

        if($this->user->checkLogin()){
          // user exists
          $this->headerData['user'] = $this->user->checkLogin();
          $this->headerData['logged_in'] = true;
        } else {
          $this->headerData['logged_in'] = false;
        }
    }

    public function index() // overview
    {

        $this->load->model('booklist','',TRUE);

        $resData = array();

        if($this->input->post('add_searchQuery')){
            $resData['query'] = htmlspecialchars($this->input->post('add_searchQuery'));

            $this->booklist->search($this->input->post('add_searchQuery'));
            $results = $this->booklist->get();
            if(!$results){
                $resData['result_rows'] = 0;
            } else {
                $resData['results'] = $results;
                $resData['result_rows'] = $this->booklist->rows;

            }

            $url = 'https://api.bol.com/catalog/v4/search?apikey=7066B13FF7DA4CD796BC6D8BB4511906&ids=8299&format=json&q='; // 
        
            $url = $url.$resData['query'];
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            $data = curl_exec($curl);
            curl_close($curl);

            $data = json_decode($data,true);

            

            if($data['totalResultSize'] > 0){
                $i = $resData['result_rows'];

                foreach($data['products'] as $product){
                    $resData['results'][$i] = array();
                    $resData['results'][$i]['site_id'] = 0;
                    $resData['results'][$i]['title'] = isset($product['title']) ? $product['title'] : '';
                    $resData['results'][$i]['subtitle'] = isset($product['subtitle']) ? $product['subtitle'] : '';
                    $resData['results'][$i]['author'] = isset($product['specsTag']) ? $product['specsTag'] : '';
                    $resData['results'][$i]['description'] = isset($product['longDescription']) ? : '';
                    $resData['results'][$i]['image_url'] = isset(end($product['images'])['url']) ? end($product['images'])['url'] : '';

                    $resData['results'][$i]['isbnlist'] = array(array('name'=>'ISBN','value'=>$product['ean']));

                    $resData['results'][$i]['bolid'] = $product['id'];


                    $i++;
                }

                $resData['result_rows'] = $i;

            }

            $this->headerData['TITLE'] = $resData['query'] . lang('title_seperator') . lang('site_name');

        } else {
            $resData['query'] = '';
            $this->headerData['TITLE'] = lang('book_add_title') . lang('title_seperator') . lang('site_name');
        }




        $this->load->view('template/header',$this->headerData);
        $this->load->view('placeadvertisement_results', $resData);
        $this->load->view('template/footer');
    }

    public function api($what,$q){
        $url = 'https://api.bol.com/catalog/v4/products/'.$q.'?apikey=7066B13FF7DA4CD796BC6D8BB4511906&format=json&ids=8299&q=';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $data = curl_exec($curl);
        curl_close($curl);

        $data = json_decode($data,true);
        

        if($data['products'][0] != NULL){


            $product = $data['products'][0];


            $this->db->select('id');
            $bookExists = $this->db->get_where('book',array('bolid'=>$product['id']));


            if($bookExists->num_rows() == 0){


                $bookInfo = array();
                $bookInfo['site_id'] = $this->config->item('site_id');
                $bookInfo['title'] = isset($product['title']) ? $product['title'] : '';
                $bookInfo['subtitle'] = isset($product['subtitle']) ? $product['subtitle'] : '';
                $bookInfo['author'] = isset($product['specsTag']) ? $product['specsTag'] : '';
                $bookInfo['description'] = isset($product['longDescription']) ? $product['longDescription'] : '';
                $bookInfo['image'] = isset(end($product['images'])['url']) ? downloadFile(end($product['images'])['url']) : '';
                $bookInfo['slug'] = toAscii($bookInfo['title']).'-'.random_string('numeric', 16);
                $bookInfo['bolid'] = $product['id'];

                if($this->db->insert('book',$bookInfo)){

                    $bookId = $this->db->insert_id();

                    // add isbn list
                    $isbnList = array($product['ean']); // more later
                    $rowInfo = array();
                    $rowInfo['site_id'] = $this->config->item('site_id');
                    $rowInfo['book_id'] = $bookId;

                    foreach($isbnList as $isbn){
                        $rowInfo['name'] = 'ISBN';
                        $rowInfo['value'] = $isbn;
                        if($rowInfo['value'] != '')
                        $this->db->insert('book_isbn',$rowInfo);
                    }


                    redirect('addbook/add/'.$bookId.'/?'.$what, 'location', 307);

                } else {
                    redirect('addbook', 'location', 307);
                }

            } else {
                $book = $bookExists->row_array();
                redirect('addbook/add/'.$book['id'].'/?'.$what, 'location', 307);
            }

        }
    }

    public function manually(){

        $this->form_validation->set_rules('title', 'lang:book_title', 'trim|callback_check_booktitle|required');
        $this->form_validation->set_rules('isbn', 'lang:book_isbn', 'trim|alpha_dash');
        $this->form_validation->set_message('check_booktitle', lang('book_isbn_error'));

        if($this->form_validation->run()){
            $bookInfo = array();
            $bookInfo['site_id'] = $this->config->item('site_id');
            $bookInfo['title'] = $this->input->post('title');
            $bookInfo['image'] = random_string('alnum', 45).'.png';
            $bookInfo['slug'] = toAscii($bookInfo['title']).'-'.random_string('numeric', 16);

            if($this->db->insert('book',$bookInfo)){

                $bookId = $this->db->insert_id();

                // add isbn list
                $isbnList = array($this->input->post('isbn')); // more later
                $rowInfo = array();
                $rowInfo['site_id'] = $this->config->item('site_id');
                $rowInfo['book_id'] = $bookId;

                foreach($isbnList as $isbn){
                    $rowInfo['name'] = 'ISBN';
                    $rowInfo['value'] = $isbn;
                    if($rowInfo['value'] != '')
                    $this->db->insert('book_isbn',$rowInfo);
                }

                

                $this->load->library('email');
                $this->email->from(lang('settings_contact_from'), lang('settings_contact_from_name'));
                $this->email->to(lang('settings_contact_email'));

                $this->email->subject('New book added');
                $this->email->message('Hi very great person,


There is a new book added to '.site_url().' by '.$this->headerData['user']['name'].'
(email: '.$this->headerData['user']['email'].', id: '.$this->headerData['user']['id'].').

Here are the details:
Title: '.$bookInfo['title'].'
Url: '.site_url('book/'.$bookInfo['slug']).'

Would you please consider to add an image to the database, with the following file name? Only if you manage to get some spare time, of course.
Image file name: '.$bookInfo['image'].'

Thank you so much in advance!


Kind regards and big kisses,

The NaamSurfer.nl server

TUEboeken.nl secretary.

P.s. You are great.'); 

                $this->email->send();

                redirect('addbook/add/'.$bookId.'/?'.$what, 'location', 307);

            } else {
                redirect('addbook', 'location', 307);
            }
        }

        $this->headerData['TITLE'] = lang('book_add_title') . lang('title_seperator') . lang('site_name');

        $this->load->view('template/header',$this->headerData);
        $this->load->view('placeadvertisement_addbook');
        $this->load->view('template/footer');

    }

    public function add($bookId = 0,$step=''){

        $this->load->model('bookinfo','',TRUE);

        if(!is_numeric($bookId)){
            redirect('addbook','location',301);
        } else {

            $this->headerData['TITLE'] = lang('book_add_title') . lang('title_seperator') . lang('site_name');

            $this->headerData['EXTRA_HEADER'] = '<script src="'.base_url('theme/js/placeadvertisement.js').'"></script>';

            $addData = array();

            $this->db->order_by('cOrder ASC');
            $q = $this->db->get('state');
            $a = $q->result_array();
            $sArray = array();
            foreach($a as $s){
                $sArray[$s['id']] = lang($s['languagekey']);
            }
            $addData['status'] = $sArray;

            $addData['step'] = 1;
            $addData['totalSteps'] = ($this->headerData['logged_in'])? 2 : 3;
            $addData['bookId'] = $bookId;

            $bookInfo = $this->bookinfo->get(array('id'=>$bookId),1);
            if(is_array($bookInfo)){
                $addData['bookExists'] = true;
                $addData['book'] = $bookInfo;
                $addData['step'] = 2;

                $this->form_validation->set_rules('price', 'lang:book_price', 'trim|required|greater_than[0]|less_than[300]');
                $this->form_validation->set_rules('state', 'lang:book_state', 'trim|required|integer');
                $this->form_validation->set_rules('coursecode', 'lang:book_coursecode', 'trim');
                $this->form_validation->set_rules('description', 'lang:book_description', 'required');
                

                if($this->form_validation->run()){

                    $addData['step'] = 3;

                    if(!$this->headerData['logged_in'] && $this->input->post('login_email')){
                        // login
                        $addData['page'] = 'login';

                        $this->load->library('form_validation');

                        $this->form_validation->set_rules('login_email', 'lang:home_login_email', 'trim|required|xss_clean|valid_email');
                        $this->form_validation->set_rules('login_password', 'lang:home_login_password', 'trim|required|xss_clean|callback_check_database');

                        if($this->form_validation->run() != FALSE){
                            $this->headerData['user'] = $this->user->checkLogin();
                            $this->headerData['logged_in'] = true;
                        }
                    } elseif(!$this->headerData['logged_in']) {
                        // register
                        $addData['page'] = 'register';
                        $addData['showRegisterErrors'] = 1;
                        $this->form_validation->set_rules('name', 'lang:home_login_name', 'trim|required');
                        $this->form_validation->set_rules('email', 'lang:home_login_email', 'trim|required|xss_clean|is_unique[user.email]|valid_email');
                        $this->form_validation->set_rules('password', 'lang:home_login_password', 'required|min_length[5]');
                        $this->form_validation->set_rules('repeat', 'lang:home_login_repeat', 'required|xss_clean|matches[password]');

                        $this->form_validation->set_message('is_unique', lang('home_login_email_exists'));

                        if($this->form_validation->run() != FALSE){
                            //register our new user :-)
                            $this->user->register($this->input->post('name'),$this->input->post('password'),$this->input->post('email'));

                            // login the user
                            if($this->check_database($this->input->post('password'))){

                                $this->headerData['user'] = $this->user->checkLogin();
                                $this->headerData['logged_in'] = true;

                            }
                        }
                    }

                    if($this->headerData['logged_in']){
                        // add advertisement
                        $adInfo = array();
                        $adInfo['site_id'] = $this->config->item('site_id');
                        $adInfo['user_id'] = $this->headerData['user']['id'];
                        $adInfo['book_id'] = $bookId;
                        $adInfo['isforsale'] = ($this->input->post('adtype') == 1)? 1 : 0;
                        $adInfo['price'] = $this->input->post('price');
                        $adInfo['state_id'] = $this->input->post('state');
                        $adInfo['status_id'] = $this->config->item('default_advertisement_status');
                        $adInfo['description'] = $this->input->post('description');

                        if($this->db->insert('advertisement',$adInfo)){

                            $ad_id = $this->db->insert_id();

                            $userData = array();
                            $userData['advertisement'] = $adInfo;
                            $userData['book'] = $bookInfo;
                            $this->bigdata->add('placeadvertisement',$this->headerData['user']['id'],$userData);


                            // add course codes (TU/e)
                            $courseList = explode(',',$this->input->post('coursecode')); // more later
                            $rowInfo = array();
                            $rowInfo['book_id'] = $bookId;

                            foreach($courseList as $course){
                                $rowInfo['code'] = trim(htmlspecialchars($course));
                                $rowInfo['user_id'] = $this->headerData['user']['id'];
                                $rowInfo['book_id'] = $bookId;
                                if($rowInfo['code'] != ''){
                                    $exists = $this->db->get_where('tue_book_course',array('book_id'=>$bookId,'code'=>$rowInfo['code']));
                                    if($exists->num_rows() == 0){
                                        $this->db->insert('tue_book_course',$rowInfo);
                                    }
                                }
                            }

                            // succes, redirect
                            redirect('addbook/success/'.$ad_id, 'location', 307); // can be changed to the book later?

                        }
                    }

                }


            } else {
                redirect('addbook', 'location', 307);
            }


            $this->load->view('template/header',$this->headerData);
            $this->load->view('placeadvertisement',$addData);
            $this->load->view('template/footer');
        }
    }

    function success($offerId){
        $this->load->model('bookinfo','',TRUE);
        if($this->headerData['logged_in']){
            $this->db->where_in('status_id',array(1,3)); // only books that are for sale or reserved
            $offerRes = $this->db->get_where('advertisement', array('id'=>$offerId));

            if($offerRes->num_rows() == 1){
                $adInfo = array();
                $adInfo['offer'] = $offerRes->row_array();

                $bookInfo = $this->bookinfo->get(array('id'=>$adInfo['offer']['book_id']),1);

                if(is_array($bookInfo)){
                    $adInfo['book'] = $bookInfo;
                    $adInfo['offer']['url'] = site_url('book/'.$adInfo['book']['slug'].'/'.$offerId);
                    $adInfo['book']['url'] = site_url('book/'.$adInfo['book']['slug']);

                    $this->headerData['TITLE'] = lang('place_ad_success_page_title') . lang('title_seperator') . lang('site_name');

                    $this->load->view('template/header',$this->headerData);
                    $this->load->view('placeadvertisement_success',$adInfo);
                    $this->load->view('template/footer');

                } else { // shouldn't happen, just send to profile
                    redirect('profile','location',307);
                }
            }

        } else {
            redirect('login','location',307);
        }
    }

    function check_booktitle($t){
        if(strlen($t) <= 2){
            $this->form_validation->set_message('check_booktitle', lang('book_title_error'));
            return false;
        } else {
            return true;
        }
    }

    function check_database($password){
       //Field validation succeeded.  Validate against database
       $username = $this->input->post('login_email');

       //query the database
       $result = $this->user->login($username, $password);

       if($result)
       {
           $sess_array = array(
             'uuid' => $result['password'],
             'email' => $result['email']
           );
           $this->session->set_userdata('logged_in', $sess_array);

            // big data
            $userData = array();
            $this->bigdata->add('login',$result['id']);

         return TRUE;
       }
       else
       {
         $this->form_validation->set_message('check_database', lang('home_login_error','alert alert-danger',true));
         return false;
       }
     }


}