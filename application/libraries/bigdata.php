<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class bigdata {

    function __construct(){
        $this->ci =& get_instance();
    }

    public function add($action,$user_id,$data=array())
    {
        $userData = array();
        $userData['user_id'] = $user_id;
        $userData['action'] = $action;
        $userData['ip'] = $_SERVER['REMOTE_ADDR'];
        $userData['browser'] = $_SERVER['HTTP_USER_AGENT'];
        $userData['header'] = json_encode($_SERVER);
        $userData['site_id'] = $this->ci->config->item('site_id');
        $userData['data'] = json_encode($data);
        $this->ci->db->insert('user_data',$userData);
    }
}

/* End of file bigdata.php */