<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home"; 
$route['404_override'] = 'home/notfound';
$route['book/delete/(:num)'] = 'book/delete/$1';
$route['book/([a-zA-Z0-9\-\_]+)'] = "book/view/$1";
$route['book/([a-zA-Z0-9\-\_]+)/(:num)'] = "book/viewOffer/$1/$2";
$route['book/([a-zA-Z0-9\-\_]+)/(:num)/edit'] = "book/viewOffer/$1/$2/edit";
$route['profile/(:num)'] = "profile/index/$1";
$route['search/([^/]+)/(:num)'] = "book/search/$1/$2";
$route['search/([^/]+)'] = "book/search/$1";
$route['contact'] = "home/contact";
$route['login'] = "home/login";
$route['register'] = "home/register";
$route['register/(:any)'] = "home/register/$1";
$route['boeken-A-Z'] = "book";
$route['boeken-A-Z/(:any)'] = "book/index/$1";
$route['about'] = "home/about";
$route['terms-and-conditions'] = "home/terms";
$route['sitemap'] = "home/sitemap";

$route['boek/(:any)'] = "redirect/book/$1";
$route['over'] = "redirect/over";
$route['home'] = "redirect/home";
$route['verkopen'] = "redirect/addbook";
$route['verkopen/(:any)'] = "redirect/addbook";

/* End of file routes.php */
/* Location: ./application/config/routes.php */