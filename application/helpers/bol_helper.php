<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function bol_url($isbn){

    $url = 'https://api.bol.com/catalog/v4/search?apikey=7066B13FF7DA4CD796BC6D8BB4511906&format=json&ids=8299&q=';

    $url = $url.$isbn;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    $data = curl_exec($curl);
    curl_close($curl);

    $data = json_decode($data,true);

    if($data['totalResultSize'] == 0) {
        return false;
    } else {

        $info = array();
        $info['url'] = $data['products'][0]['urls'][0]['value'];
        $info['url'] = 'https://partnerprogramma.bol.com/click/click?p=1&t=url&s=27138&f=TXL&url='.urlencode($info['url']).'&name=BookPageLeiden';

        if($data['products'][0]['offerData']['bolCom'] == 0){
            return false;
        } else {
            setlocale(LC_MONETARY, 'en_US');
            $info['price'] = money_format('%i', $data['products'][0]['offerData']['offers'][0]['price']);
            return $info;
        }

        
    }

}

function bol_ebook($isbn, $title){

    $url = 'https://api.bol.com/catalog/v4/search?apikey=7066B13FF7DA4CD796BC6D8BB4511906&format=json&ids=7419&q=';

    $url = $url.$isbn;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_HEADER, false);
    $data = curl_exec($curl);
    curl_close($curl);

    $data = json_decode($data,true);

    if(($data['totalResultSize'] == 0) || $title != $data['products'][0]['title']) {
        return false;
    } else {

        $info = array();
        $info['url'] = $data['products'][0]['urls'][0]['value'];
        $info['url'] = 'https://partnerprogramma.bol.com/click/click?p=1&t=url&s=27138&f=TXL&url='.urlencode($info['url']).'&name=BookPageLeiden';

        if($data['products'][0]['offerData']['bolCom'] == 0){
            return false;
        } else {
            setlocale(LC_MONETARY, 'en_US');
            $info['price'] = money_format('%i', $data['products'][0]['offerData']['offers'][0]['price']);
            return $info;
        }

        
    }

}