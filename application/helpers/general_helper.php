<?php

	function bookHTML($books = array()){
		$return = '';

		if(count($books) > 0){ 
			foreach($books as $book){
			  $return .= '
				  <div class="col-sm-6">
				    <div class="row">
				      <div class="col-sm-4"><a href="'.$book['url'].'"><img src="'.bookImage($book['image'], array(150, 180)).'" class="img-responsive img-book"/></a></div>
				      <div class="col-sm-8">
				        <h3 style="margin-top: 0;"><a href="'.$book['url'].'">'.$book['title'].'</a></h3>
				        <p><em>'.$book['subtitle'].'</em></p>
				        <p>'.$book['shortdescription'].'</p>
				        <p><a href="'.$book['url'].'" class="btn btn-primary btn-sm">'.lang('offers').' <span class="badge">'.$book['offered'].'</a>
				          <a href="'.$book['url'].'" class="btn btn-default btn-sm">'.lang('requests').' <span class="badge">'.$book['requested'].'</a></p>
				      </div>
				    </div>
				  </div>
			  ';
			} 
		}

		return $return;
	}

	function base64_url_encode($input) {
	 return strtr(base64_encode($input), '+/=', '-_,');
	}

	function base64_url_decode($input) {
	 return base64_decode(strtr($input, '-_,', '+/='));
	}

	function currentPageUrl() {
		$link =  "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$escaped_link = htmlspecialchars($link, ENT_QUOTES, 'UTF-8');
		return $escaped_link;
	}
	
	function user_icon(){
		$CI =& get_instance();
		return (!$CI->session->userdata('img')) ? base_url('theme/img/round-icons/free-60-icons-12.png') : $CI->session->userdata('img');

	}

	function bookImage($img, $size = array(150, 200)){
		//var_dump(file_exists(FCPATH.'/assets/images/'.$img));
		if(is_image(FCPATH.'/assets/images/'.$img)){

			return base_url('assets/crop.php?w='.$size[0].'&h='.$size[1].'&q=100&zc=1&src='.base_url('assets/images/'.$img));

		}else{

			return base_url('assets/crop.php?w='.$size[0].'&h='.$size[1].'&q=100&zc=1&src='.base_url('assets/notfound.png'));			

		}
	}

	function is_image($path){
		if(file_exists($path)){
		    $a = @getimagesize($path);
		    $image_type = $a[2];
		     
		    if(in_array($image_type , array(IMAGETYPE_GIF , IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
		    {
		        return true;
		    }
		    return false;
		}else{
			return false;
		}
	}
		
	function userLink($id){
		$CI =& get_instance();
		$res = $CI->db->get_where('user', array('id' => $id));
		if($res->num_rows == 0) return '';
		
		$data = $res->row_array();
		
		$current = $CI->user->checkLogin();
		if($id == $current['id']){
			$url = site_url('profile');
			$x = ' <span class="text-muted">('.lang('me').')</span>';

		}else{
			$url = site_url('profile/'.$id);
			$x = '';

		}
		
		return '<a href="'.$url.'">'.$data['name'].$x.'</a>';

	}
	
	function userCourse($id){
		$CI =& get_instance();
		$res = $CI->db->get_where('user', array('id' => $id));
		if($res->num_rows == 0) return '';
		$d = $res->row_array();
		
		$res = $CI->db->get_where('courses', array('id' => $d['courses_id']));
		if($res->num_rows == 0) return '';
		
		$data = $res->row_array();
		
		return $data['name'];

	}
	
	function userMail($id){
		$CI =& get_instance();
		$res = $CI->db->get_where('user', array('id' => $id));
		if($res->num_rows == 0) return '';
		
		$data = $res->row_array();
		
		return $data['email'];

	}

	function downloadFile($url) {
	  $file_name = mt_rand().basename($url);
	  $path = FCPATH.'/assets/images/'.$file_name;
	  $newfname = $path;
	  $file = fopen ($url, "rb");
	  if ($file) {
	    $newf = fopen ($newfname, "wb");

	    if ($newf)
	    while(!feof($file)) {
	      fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
	    }
	  }

	  if ($file) {
	    fclose($file);
	  }

	  if ($newf) {
	    fclose($newf);
	  }

	  return $file_name;
	}


function toAscii($str) {
	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $str);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace("/[\/_|+ -]+/", '-', $clean);

	return $clean;
}