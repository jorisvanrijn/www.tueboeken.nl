<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function loginForm(){
	return '

	            <div style="margin-bottom: 25px" class="input-group">
	                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
	                <input id="login-username" type="text" class="form-control" name="login_email" value="" placeholder="'.lang('home_login_email').'">
	            </div>

	            <div style="margin-bottom: 25px" class="input-group">
	                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
	                <input id="login-password" type="password" class="form-control" name="login_password" placeholder="'.lang('home_login_password').'">
	            </div>


	            <div style="margin-top:10px" class="form-group">
	                <!-- Button -->

	                <div class="col-sm-12 controls">
	                    <input type="submit" id="btn-login" value="Login" class="btn btn-primary">&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="'.site_url('home/forgot').'" class="text-muted" style="font-size: 11px;">'.lang('home_login_forgot').'</a>

	                </div>
	            </div>

	';
}

function salt_password($password){
    $key = '4TDe9XyBes3vlV6N0miTfFgjMg2xR4f2';
    
    return sha1($key.$password);
}

function getCourseDropDown($id = 0){
      $CI =& get_instance();

      $q = $CI->db->get('courses');
      $c = $q->result_array();

      $r = '';
      foreach($c as $k){
            $r .= '<option value="'.$k['id'].'"'.(($id == $k['id']) ? ' selected="selected"' : '').'>'.$k['name'].'</option>';
      }

      return $r;
}

function registerForm($showErrors=false){
	return '

		<div class="form-group" style="height:35px;">
                <label for="inputName" class="col-sm-2 control-label">'.lang('home_login_name').'</label>
                <div class="col-sm-10">
                    <input type="text" value="'.set_value('name').'" class="form-control" id="inputName" name="name" placeholder="'.lang('home_login_nameplaceholder').'" data-validate="validateName">
                    '.(($showErrors)? form_error('name','<div class="alert alert-danger">','</div>') : '').'
                </div>
            </div>
            <div style="clear:both"></div>

            <hr>

            <div class="form-group" style="height:35px;">
                <label for="inputName" class="col-sm-2 control-label">'.lang('home_login_study').'</label>
                <div class="col-sm-10">
                    <select name="course_id" class="select-formc advselect form-control" id="a29851576">
                        '.getCourseDropDown().'
                    </select>
                </div>
            </div>
            <div style="clear:both"></div>

            <hr/>

            <div class="form-group" style="height:80px;">
                <label for="inputEmail" class="col-sm-2 control-label">'.lang('home_login_email').'</label>
                <div class="col-sm-10">
                    <input type="email" value="'.set_value('email').'" class="form-control" id="inputEmail" name="email" placeholder="'.lang('home_login_emailplaceholder').'" data-validate="validateEmail">
                    <em class="text-muted" style="font-size: 12px; margin-top: 3px;">'.lang('home_login_emailsub').'</em>
                    '.(($showErrors)? form_error('email','<div class="alert alert-danger">','</div>') : '').'
                </div>
            </div>
            <div style="clear:both"></div>

            <hr/>

            <div class="form-group" style="height:35px;">
                <label for="inputPassword" class="col-sm-2 control-label">'.lang('home_login_password').'</label>
                <div class="col-sm-10">
                    <input type="password" value="'.set_value('password').'" class="form-control" id="inputPassword" name="password" placeholder="'.lang('home_login_passwordplaceholder').'" data-validate="validatePassword">
                    '.(($showErrors)? form_error('password','<div class="alert alert-danger">','</div>') : '').'
                </div>
            </div>
            <div style="clear:both"></div>

            <div class="form-group" style="height:35px;">
                <label for="inputRepeat" class="col-sm-2 control-label">'.lang('home_login_repeat').'</label>
                <div class="col-sm-10">
                    <input type="password" value="'.set_value('repeat').'" class="form-control" id="inputRepeat" name="repeat" placeholder="'.lang('home_login_repeatplaceholder').'" data-validate="validateRepeat">
                    '.(($showErrors)? form_error('repeat','<div class="alert alert-danger">','</div>') : '').'
                </div>
            </div>
            <div style="clear:both"></div>
            <hr/>
            <div style="margin-top:10px" class="form-group">
                <!-- Button -->

                <div class="col-sm-12 controls text-right">
                    <input type="submit" value="'.lang('home_login_submit').' &raquo;" class="btn btn-primary">
                </div>
            </div>

	';
}