<?php

	header("Content-Type:text/plain");

	function doQuery($q, $c){
		$q = $q;
		$res = $c->query($q);

		if(!$res){
			echo $c->error.PHP_EOL;
			return false;
		}else{
			return $res;
		}
	}

	function safe($s, $c){
		return $c->real_escape_string($s);
	}

	function downloadFile($url, $path) {

	  $newfname = $path;
	  $file = fopen ($url, "rb");
	  if ($file) {
	    $newf = fopen ($newfname, "wb");

	    if ($newf)
	    while(!feof($file)) {
	      fwrite($newf, fread($file, 1024 * 8 ), 1024 * 8 );
	    }
	  }

	  if ($file) {
	    fclose($file);
	  }

	  if ($newf) {
	    fclose($newf);
	  }
	}

	// End o' functions

	$oC = new mysqli('localhost', 'tueboeken_user', 'ilORiV5DSv', 'tueboeken_main');
	$nC = new mysqli('localhost', 'tueboeken_db', 'GbN0EUmHI', 'tueboeken_db');

	if($nC->connect_error){
		echo $nC->connect_error;
	}else{

		/* 
		** tables to transfer:
		** ads, book (isbn)
		** replies, searches
		*/

		// bookrecord > advertisement
		echo ':::: ADS ::::'.PHP_EOL;
		$res = doQuery('SELECT * FROM `bookrecord`', $oC);
		echo $res->num_rows .' rows found in OLD `bookrecord`'.PHP_EOL;

		doQuery('TRUNCATE `advertisement`', $nC);
		echo '`advertisement` is emptied'.PHP_EOL;

		$count = 0;
		while($row = $res->fetch_array(MYSQLI_ASSOC)){
			doQuery("INSERT INTO `advertisement` (`id`, `site_id`, `book_id`, `user_id`, `isforsale`, `price`, `status_id`, `state_id`, `description`, `created`) VALUES
				(".safe($row['id'], $nC).", 1, ".safe($row['book_id'], $nC).", ".safe($row['account_id'], $nC).", ".safe($row['forsale'], $nC).", ".safe($row['price'], $nC).", ".safe($row['status_id'], $nC).", ".safe($row['staat_id'], $nC).", '".safe($row['desc'], $nC)."', '".safe($row['date'], $nC)."');
			", $nC);
			$count++;
		}

		echo '`advertisement` is filled with '.$count.' rows'.PHP_EOL.PHP_EOL;

		// book > book and book_isbn
		echo ':::: BOOKS ::::'.PHP_EOL;
		$res = doQuery('SELECT * FROM `book`', $oC);
		echo $res->num_rows .' rows found in OLD `book`'.PHP_EOL;

		doQuery('TRUNCATE `book`', $nC);
		echo '`book` is emptied'.PHP_EOL;
		doQuery('TRUNCATE `book_isbn`', $nC);
		echo '`book_isbn` is emptied'.PHP_EOL;

		$count = 0;
		$count2 = 0;
		while($row = $res->fetch_array(MYSQLI_ASSOC)){
			$url = str_replace(array('http://tueboeken.nl/modules/crop.php?w=340&amp;h=400&amp;zc=1&amp;q=100&amp;a=m&amp;src=',
						'http://tueboeken.nl/template/images/noimage.png'),
						'', $row['imageurl']);

			$file_name = basename($url);
			downloadFile($url, '../assets/images/'.$file_name);

			doQuery("
				INSERT INTO `book`(`id`, `site_id`, `slug`, `title`, `subtitle`, `author`, `description`, `image`, `googleid`, `publisher`) VALUES 
				(".safe($row['id'], $nC).",
				1,
				'".safe($row['location'], $nC)."',
				'".safe($row['title'], $nC)."',
				'".safe($row['subtitle'], $nC)."',
				'".safe($row['author'], $nC)."',
				'".safe($row['desc'], $nC)."',
				'".safe($file_name, $nC)."',
				'".safe($row['google_id'], $nC)."',
				'".safe($row['uitgever'], $nC)."')
			", $nC);
			$book_id = $nC->insert_id;
			$count++;

			$isbn = (json_decode(htmlspecialchars_decode($row['ISBN']), true));
			foreach($isbn as $a){
				doQuery("
					INSERT INTO `book_isbn` (`site_id`, `book_id`, `name`, `value`) VALUES
					(
						1,
						".$book_id.",
						'".safe($a['type'], $nC)."',
						'".safe($a['identifier'], $nC)."'
					)", $nC);
				$count2++;
			}

			
		}

		echo '`book` is filled with '.$count.' rows'.PHP_EOL;
		echo '`book_isbn` is filled with '.$count2.' rows'.PHP_EOL.PHP_EOL;
		
		
	}

?>